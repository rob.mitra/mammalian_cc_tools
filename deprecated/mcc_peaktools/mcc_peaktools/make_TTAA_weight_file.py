"""
make_TTAA_weight_file.py 

written 11/17/16 by RDM

This script is the first in a series of scripts that will be used for background
free significance calling.  

This file takes in a genome (e.g. mm10 or Hg19) and gives the locations of all of the 
TTAA's.  Since TTAA is palindromic, it will only report the positions on the sense strand.
It will report positions on all numbered chromosomes and X, Y, M, but not 1_GL456210_random
for example.  


For now (11/17/16), all TTAA's are assigned a weight of one.  In the future, it may be advantageous
to weight the TTAA's based on their genomic position relative to say coding regions.

Usage 
python make_TTAA_weight_file.py -g <genome path and filename>  -o <output path and filename>

Input file format.  I'm using the star genome format which is >1 then a whole bunch of text 
The text can be upper or lower case and have N's

Tested on 11/17/16
checked for grabbing x,y,m
checked for not grabbing 11_GL456210_random
checked for position (1 indexed output)

"""
from Bio import SeqIO
from Bio import Seq
import argparse
import re


def make_TTAA_weight_file(inputfile,outputfile):

	#open files

	in_filehandle = open(inputfile,'rU')
	out_filehandle = open(outputfile,'w')

	#read in first fasta record
	for seq_record in SeqIO.parse(in_filehandle,"fasta"):
		match_flag = 0
		#is it a numbered chromosome or X,Y,M?
		match_test = re.match(r"^\d+$",seq_record.name,re.I)
		if match_test:
			match_flag = 1
		else:
			match_test = re.match(r"^M$",seq_record.name,re.I)
			if match_test:
				match_flag = 1
			else:
				match_test = re.match(r"^X$",seq_record.name,re.I)
				if match_test:
					match_flag = 1
				else:
					match_test = re.match(r"^Y$",seq_record.name,re.I)
					if match_test:
						match_flag = 1
		if match_flag:
			chr = match_test.group()
			#if so, find all TTAAs and print their locations to output files

			#convert to upper case
			seq_record.seq = seq_record.seq.upper()
			start_coor_zi = seq_record.seq.find("TTAA")
			while start_coor_zi != -1:
				#output coord, start
				print >> out_filehandle,chr+"\t"+str(start_coor_zi+1)+"\t"+str(1)
				start_coor_zi = seq_record.seq.find("TTAA",start_coor_zi+4)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='cc_filter_reads.py')
    parser.add_argument('-g','--genome',help='genome fasta file path and name',required=True)
    parser.add_argument('-o','--output',help='output filename (full path)',required=True)
    args = parser.parse_args()
    make_TTAA_weight_file(args.genome,args.output)


	

	



