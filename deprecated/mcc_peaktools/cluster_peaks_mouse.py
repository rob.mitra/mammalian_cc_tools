#!/usr/bin/env python
"""
cluster_peaks_mouse.py
written July 2017 by RDM
    '-e','--expfile',help='Filename of experiment gnashyfile',required=True
    '-b','--bgfile',help='Filename of background gnashyfile',required=True
    '-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt'
    '-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt'
    '-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed'
    '-g','--genome',help='genome tpe, mm10 or hg19 are supported',default = mm10
    '-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4
    '-cs','--cluster_size',help='target cluster size',required=False,default=1000
    '-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2
    '-ms','--macsstyle',help='flag for macs style significance calling or just under cluster',default = True
This script identify significant TF binding sites from mammalian calling card data collected with a TF-piggyBac
transposase fusion.  It borrows heavily from MACS (Zhang, Liu et al Genome Biol 2008), the most popular peak
caller for ChIP-Seq data, but makes some modifications specific for Calling Card data.

The workflow is as follows.  
1.  Cluster calling card insertions.  Hierachical clustering is performed to cluster calling card peaks.  See comments in code for implementation details.  
2.  Call significant peaks.  Candidate peaks are assigned a p-value by testing the observed number of hops in the peak against 
the null model, which is a Poisson distribution parameterized by the background hops in the region.  As in MACS, 4 parameters are 
calculated, Lambda_bg, Lambda_1k, Lambda_5k, Lambda_10k, by using the background hops that are under the peak (bg), the background hops that are
in a 1kb window around the peak center (1k), or a 5kb window (5kb) or a 10kb window (10kb).  These values 
are normalized to the number of TTAAs in each window, and then the maximum of this value is used for the null model.  If the macsstyle flag is false lambda_bg is used.

3.  Annotate Peaks.  Peaks are annotated using bedtools to provide information about nearby genes. 



changes:


"""

import mcc_peaktools
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='find_peaks_mouse.py')
    parser.add_argument('-e','--expfile',help='Filename of experiment gnashyfile',required=True)
    parser.add_argument('-b','--bgfile',help='Filename of background gnashyfile',required=True)
    parser.add_argument('-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt')
    parser.add_argument('-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt')
    parser.add_argument('-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed')
    parser.add_argument('-g','--genome',help='genome type, mm10 or hg19 are supported',required=False,default = "mm10")
    parser.add_argument('-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4)
    parser.add_argument('-cs','--cluster_size',help='target cluster size',required=False,default=1000)
    parser.add_argument('-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2)
    parser.add_argument('-ms','--macsstyle',help='flag for macs style significance calling or just under cluster',default = True)
    args = parser.parse_args()
    mcc_peaktools.cluster_and_annotate(args.expfile,args.bgfile,args.outputfile,args.TTAAfile,args.annotationfile,args.genome,
    	float(args.pvalue_cutoff),int(args.cluster_size),float(args.pseudocounts),bool(args.macsstyle))






