#!/usr/bin/env python
"""
find_peaks_mouse_bf.py
written July 2017 by RDM
    '-e','--expfile',help='Filename of experiment gnashyfile',required=True
    '-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt'
    '-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt'
    '-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed'
    '-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4
    '-pfc','--peakfinder_cutoff',help='peakfinder significance cutoff',required=False,default=1e-3
    '-w','--window_size',help='peakfinder window size',required=False,default=1000
    '-s','--step_size',help='peakfinder window size',required=False,default=500
    '-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2
    '-g','--genome',help='genome type, mm10 or hg19 are supported',default='mm10'
This script identify significant TF binding sites from mammalian calling card data collected with a TF-piggyBac
transposase fusion.  It borrows heavily from MACS (Zhang, Liu et al Genome Biol 2008), the most popular peak
caller for ChIP-Seq data, but makes some modifications specific for Calling Card data.

The workflow is as follows.  
1.  Find candidate peaks.  A window of user defined size is slid across the genome to identify regions
that have significantly more experimental hops than background hops as determined by the Poisson distribution
parameterized with the background hops (a user-defined cutoff, --peakfinder_cutoff defines what is meant by significant).
Overlapping significant windows are merged and the peak center is identified by taking the median location of all insertions.

2.  Call significant peaks.  Candidate peaks are assigned a p-value by testing the observed number of hops in the peak against 
the null model, which is a Poisson distribution parameterized by the background hops in the region.  As in MACS, 4 parameters are 
calculated, Lambda_bg, Lambda_1k, Lambda_5k, Lambda_10k, by using the background hops that are under the peak (bg), the background hops that are
in a 1kb window around the peak center (1k), or a 5kb window (5kb) or a 10kb window (10kb).  These values 
are normalized to the number of TTAAs in each window, and then the maximum of this value is used for the null model.  

3.  Annotate Peaks.  Peaks are annotated using bedtools to provide information about nearby genes. 



changes:


"""

from mcc_peaktools import *
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='find_peaks_mouse.py')
    parser.add_argument('-e','--expfile',help='Filename of experiment gnashyfile',required=True)
    parser.add_argument('-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt')
    parser.add_argument('-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt')
    parser.add_argument('-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed')
    parser.add_argument('-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4)
    parser.add_argument('-pfc','--peakfinder_cutoff',help='peakfinder significance cutoff',required=False,default=1e-3)
    parser.add_argument('-w','--window_size',help='peakfinder window size',required=False,default=1000)
    parser.add_argument('-s','--step_size',help='peakfinder window size',required=False,default=500)
    parser.add_argument('-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2)
    parser.add_argument('-g','--genome',help='genome type, mm10 or hg19 are supported',required=False,default = "mm10")

    args = parser.parse_args()
    mcc_peaktools.call_peaks_and_annotate_bf(args.expfile,args.outputfile,args.TTAAfile,args.annotationfile,args.genome,
    	float(args.pvalue_cutoff),float(args.peakfinder_cutoff),int(args.window_size),int(args.step_size),float(args.pseudocounts))






