#!/usr/bin/env python
"""
find_peaks_human.py
written June 2017 by RDM
  	'-e','--expfile',help='Filename of experiment gnashyfile',required=True
    '-b','--bgfile',help='Filename of background gnashyfile',required=True
    '-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt'
    '-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/TTAA_pos_hg19.txt'
    '-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed'
    '-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4
    '-pfc','--peakfinder_cutoff',help='peakfinder significance cutoff',required=False,default=1e-3
    '-w','--window_size',help='peakfinder window size',required=False,default=1000
    '-s','--step_size',help='peakfinder window size',required=False,default=500
    '-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2
    '-g','--genome',help='genome type, mm10 or hg19 are supported',default = 'hg19'
This script identify significant TF binding sites from mammalian calling card data collected with a TF-piggyBac
transposase fusion.  It borrows heavily from MACS (Zhang, Liu et al Genome Biol 2008), the most popular peak
caller for ChIP-Seq data, but makes some modifications specific for Calling Card data.

The workflow is as follows.  
1.  Find candidate peaks.  A window of user defined size is slid across the genome to identify regions
that have significantly more experimental hops than background hops as determined by the Poisson distribution
parameterized with the background hops (a user-defined cutoff, --peakfinder_cutoff defines what is meant by significant).
Overlapping significant windows are merged and the peak center is identified by taking the median location of all insertions.

2.  Call significant peaks.  Candidate peaks are assigned a p-value by testing the observed number of hops in the peak against 
the null model, which is a Poisson distribution parameterized by the background hops in the region.  As in MACS, 4 parameters are 
calculated, Lambda_bg, Lambda_1k, Lambda_5k, Lambda_10k, by using the background hops that are under the peak (bg), the background hops that are
in a 1kb window around the peak center (1k), or a 5kb window (5kb) or a 10kb window (10kb).  These values 
are normalized to the number of TTAAs in each window, and then the maximum of this value is used for the null model.  

3.  Annotate Peaks.  Peaks are annotated using bedtools to provide information about nearby genes. 



changes:


"""


import numpy as np 
import pandas as pd
import scipy.stats as scistat
from bx.intervals.intersection import Intersecter, Interval
import pybedtools.bedtool as bt 
import argparse

#experiment_frame = pd.read_csv('M6Brd4.txt.gnashy', delimiter = "\t",header = None)
#experiment_frame.columns = ['Chr','Pos','Reads']


def compute_cumulative_poisson(exp_hops_region,bg_hops_region,total_exp_hops,total_bg_hops,pseudocounts):
	#usage
	#scistat.poisson.cdf(x,mu)
	#scales sample with more hops down to sample with less hops
	#tested 6/14/17
	if total_bg_hops >= total_exp_hops:
		return(1-scistat.poisson.cdf((exp_hops_region+pseudocounts),bg_hops_region * (float(total_exp_hops)/float(total_bg_hops)) + pseudocounts))
	else:
		return(1-scistat.poisson.cdf(((exp_hops_region *(float(total_bg_hops)/float(total_exp_hops)) )+pseudocounts),bg_hops_region + pseudocounts))

def find_peaks(experiment_gnashyframe,background_gnashyframe,TTAA_frame,pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):
	"""This program roughly follows the algorithm used by MACS to call
	ChIP-Seq peaks.  The function is passed a experiment frame (gnashy format), 
	a background frame (gnashy format), and an TTAA_frame.  It then builds interval trees containing
	all of the background and experiment hops and all of the TTAAs.  Next, it scans through the genome
	with a window of window_size and step size of step_size and looks for regions that
	have signficantly more experiment hops than background hops (poisson w/ pvalue_cutoff).
	It merges consecutively enriched windows and computes the center of the peak.  Next 
	it computes lambda, the number of insertions per TTAA expected from the background
	distribution by taking the max of lambda_bg, lamda_1, lamda_5, lambda_10.  It then computes
	a p-value based on the expected number of hops = lamda * number of TTAAs in peak * number of hops
	in peak.  Finally, it returns a frame that has Chr,Start,End,Center,Experiment Hops,
	Fraction Experiment,Background Hops,Fraction Background,Poisson pvalue

	"""
	peaks_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops",
		"Fraction Experiment","Background Hops","Fraction Background","Lambda Type",
		"Lambda","Poisson pvalue"])

	
	experiment_gnashy_dict = {}
	experiment_dict_of_trees = {}
	total_experiment_hops = len(experiment_gnashyframe)
	background_gnashy_dict = {} 
	background_dict_of_trees = {}
	total_background_hops = len(background_gnashyframe)
	TTAA_frame_gbChr_dict = {} 
	TTAA_dict_of_trees = {}
	list_of_l_names = ["bg","1k","5k","10k"]

	#group by chromosome and populate interval tree with TTAA positions
	for name,group in TTAA_frame.groupby('Chr'):
		 
		TTAA_frame_gbChr_dict[name] = group
		TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
		#initialize tree
		TTAA_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in TTAA_frame_gbChr_dict[name].iterrows():	
			TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
	#group by chromosome and populate interval tree with positions of background hops
	for name,group in background_gnashyframe.groupby('Chr'):
		background_gnashy_dict[name] = group
		background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
		#initialize tree
		background_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in background_gnashy_dict[name].iterrows():	
			background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#group by chromosome and populate interval tree with positions of experiment hops
	for name,group in experiment_gnashyframe.groupby('Chr'):
		experiment_gnashy_dict[name] = group
		experiment_gnashy_dict[name].index = experiment_gnashy_dict[name]["Pos"]
		#initialize tree
		experiment_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in experiment_gnashy_dict[name].iterrows():	
			experiment_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#these will eventually be the columns in the peaks frame that will be returned.
	chr_list = []
	start_list = []
	end_list = []
	center_list = []
	num_exp_hops_list = []
	num_bg_hops_list = []
	frac_exp_list = []
	frac_bg_list = []
	lambda_type_list =[]
	lambda_list = []
	pvalue_list = []
	
	#group experiment gnashyfile by chomosome
	for name,group in experiment_gnashyframe.groupby('Chr'):
		max_pos = max(group["Pos"])
		sig_start = 0
		sig_end = 0
		sig_flag = 0
		for window_start in range(1,max_pos+window_size,step_size):
			overlap = experiment_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_exp_hops = len(overlap)
			bg_overlap = background_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_bg_hops = len(bg_overlap)

			#is this window significant?
			if compute_cumulative_poisson(num_exp_hops,num_bg_hops,total_experiment_hops,total_background_hops,pseudocounts) < pvalue_cutoff:
				#was last window significant?
				if sig_flag:
					#if so, extend end of windows
					sig_end = window_start+window_size-1
				else:
					#otherwise, define new start and end and set flag
					sig_start = window_start
					sig_end = window_start+window_size-1
					sig_flag = 1

			else:
				#current window not significant.  Was last window significant?
				if sig_flag:
					#add full sig window to the frame of peaks 
					
					#add chr, peak start, peak end
					chr_list.append(name) #add chr to frame
					start_list.append(sig_start) #add peak start to frame
					end_list.append(sig_end) #add peak end to frame
				
					#compute peak center and add to frame
					overlap = experiment_dict_of_trees[name].find(sig_start,sig_end)
					exp_hop_pos_list = [x.start for x in overlap]
					peak_center = np.median(exp_hop_pos_list)
					center_list.append(peak_center) #add peak center to frame

					#add number of experiment hops in peak to frame
					num_exp_hops = len(overlap)
					num_exp_hops_list.append(num_exp_hops)

					#add fraction of experiment hops in peak to frame
					frac_exp_list.append(float(num_exp_hops)/total_experiment_hops)

					#add number of background hops in peak to frame
					bg_overlap = background_dict_of_trees[name].find(sig_start,sig_end)
					num_bg_hops = len(bg_overlap)
					num_bg_hops_list.append(num_bg_hops)

					frac_bg_list.append(float(num_bg_hops)/total_background_hops)		
					#find lambda and compute significance of peak
					if total_background_hops >= total_experiment_hops: #scale bg hops down
						#compute lambda bg
						num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
						lambda_bg = ((num_bg_hops*(float(total_experiment_hops)/total_background_hops))/num_TTAAs) 


						#compute lambda 1k
						num_bg_hops_1k = len(background_dict_of_trees[name].find(peak_center-499,peak_center+500))
						num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(peak_center-499,peak_center+500))
						lambda_1k = (num_bg_hops_1k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_1k,1))


						#compute lambda 5k
						num_bg_hops_5k = len(background_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						lambda_5k = (num_bg_hops_5k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_5k,1))


						#compute lambda 10k
						num_bg_hops_10k = len(background_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						lambda_10k = (num_bg_hops_10k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_10k,1))
						lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])


						#record type of lambda used
						index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
						lambda_type_list.append(list_of_l_names[index])
						#record lambda
						lambda_list.append(lambda_f)
						#compute pvalue and record it

						pvalue = 1-scistat.poisson.cdf((num_exp_hops+pseudocounts),lambda_f*num_TTAAs+pseudocounts)
						pvalue_list.append(pvalue)

					else: #scale experiment hops down
						#compute lambda bg
						num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
						lambda_bg = (float(num_bg_hops)/num_TTAAs) 



						#compute lambda 1k
						num_bg_hops_1k = len(background_dict_of_trees[name].find(peak_center-499,peak_center+500))
						num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(peak_center-499,peak_center+500))
						lambda_1k = (float(num_bg_hops_1k)/(max(num_TTAAs_1k,1)))



						#compute lambda 5k
						num_bg_hops_5k = len(background_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						lambda_5k = (float(num_bg_hops_5k)/(max(num_TTAAs_5k,1)))



						#compute lambda 10k
						num_bg_hops_10k = len(background_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						lambda_10k = (float(num_bg_hops_10k)/(max(num_TTAAs_10k,1)))
						lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])



						#record type of lambda used
						index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
						lambda_type_list.append(list_of_l_names[index])
						#record lambda
						lambda_list.append(lambda_f)
						#compute pvalue and record it
						pvalue = 1-scistat.poisson.cdf(((float(total_background_hops)/total_experiment_hops)*num_exp_hops+pseudocounts),lambda_f*num_TTAAs+pseudocounts)
						pvalue_list.append(pvalue)

					#number of hops that are a user-defined distance from peak center
					sig_flag = 0

					

				#else do nothing.
				
				
	#make frame from all of the lists
	peaks_frame["Chr"] = chr_list
	peaks_frame["Start"] = start_list
	peaks_frame["End"] = end_list
	peaks_frame["Center"] = center_list
	peaks_frame["Experiment Hops"] = num_exp_hops_list 
	peaks_frame["Fraction Experiment"] = frac_exp_list 
	peaks_frame["Background Hops"] = num_bg_hops_list 
	peaks_frame["Fraction Background"] = frac_bg_list
	peaks_frame["Lambda Type"] = lambda_type_list
	peaks_frame["Lambda"] = lambda_list
	peaks_frame["Poisson pvalue"] = pvalue_list
	return peaks_frame

def peaks_frame_to_bed(peaks_frame,bedfilename,genome_type='hg19'):
	bed_frame = peaks_frame[["Chr","Start","End"]].copy()
	bed_frame.loc[:,"Start"] = bed_frame["Start"]-1 #start coords of bed files are 0 indexed while ends are 1 indexed
	if (genome_type == 'hg18') or (genome_type == 'hg19'):
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
	else:
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
	bed_frame.loc[:,"Chr"] = [chr_dict[x] for x in bed_frame.loc[:,"Chr"]]
	bed_frame.to_csv(bedfilename,sep = "\t",header = None,index=None)


#def merge_bedfile_and_peaks_frame():

def annotate_peaks_frame(peaks_frame,refGene_filename = '/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed',genome_type='hg19'):
	"This function annotates peaks using pybedtools"

	peaks_frame_to_bed(peaks_frame,"temp_peaks.bed",genome_type)
	peaks_bed = bt.BedTool('temp_peaks.bed')
	peaks_bed = peaks_bed.sort()
	#convert peaks frame to bed
	peaks_bed = peaks_bed.closest(refGene_filename,D="ref",t="first",k=2)
	peaks_bed.saveas('temp_peaks.bed')

	#read in gene_annotation_bedfilename
	temp_annotated_peaks = pd.read_csv("temp_peaks.bed",delimiter = "\t",header=None)
	temp_annotated_peaks.columns = ["Chr","Start","End","Feature Chr","Feature Start", "Feature End","Feature sName","Feature Name","Strand","Distance"]
	temp_annotated_peaks.loc[:,"Start"] = temp_annotated_peaks["Start"]+1 #convert start coords back to 1 indexed coordinates
	index_list = [(x,y,z) for x,y,z in zip(temp_annotated_peaks["Chr"],temp_annotated_peaks["Start"],temp_annotated_peaks["End"])]

	temp_annotated_peaks.index = pd.MultiIndex.from_tuples(index_list)
	
	if (genome_type == 'hg18') or (genome_type == 'hg19'):
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
	else:
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
	peaks_frame.loc[:,"Chr"] = [chr_dict[x] for x in peaks_frame.loc[:,"Chr"]]
	
	index_list = [(x,y,z) for x,y,z in zip(peaks_frame["Chr"],peaks_frame["Start"],peaks_frame["End"])]
	peaks_frame.index = pd.MultiIndex.from_tuples(index_list)
	
	temp_list = list(peaks_frame.columns)
	temp_list = temp_list+["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand","Feature 1 Distance", 
		"Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand","Feature 2 Distance"]
	peaks_frame = peaks_frame.reindex(columns=temp_list,fill_value=0)

	for idx,row in temp_annotated_peaks.iterrows():
		if not(peaks_frame.loc[idx,"Feature 1 sName"]):
			peaks_frame.loc[idx,["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand",
			"Feature 1 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]])	
		else:
			peaks_frame.loc[idx,["Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand",
			"Feature 2 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]])
	return peaks_frame		
	

def annotate_gnashyfile(expfile,bgfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/human/TTAA_pos_hg19.txt',
	annotation_file = '/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed',genome='hg19',pvalue_cutoff=1e-4,
	peak_pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):

	"""This function is the wrapper that calls the various subfunctions"""

	expframe = pd.read_csv(expfile,delimiter="\t",header=None)
	expframe.columns = ["Chr","Pos","Reads"]

	bgframe = pd.read_csv(bgfile,delimiter="\t",header=None)
	bgframe.columns = ["Chr","Pos","Reads"]

	TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
	TTAAframe.columns = ["Chr","Pos","Reads"]

	peaks_frame = find_peaks(expframe,bgframe,TTAAframe,peak_pvalue_cutoff,window_size,step_size,pseudocounts)
	peaks_frame = annotate_peaks_frame(peaks_frame,annotation_file,genome)
	peaks_frame = peaks_frame[peaks_frame["Poisson pvalue"] <= pvalue_cutoff]
	peaks_frame = peaks_frame.sort_values(["Poisson pvalue"])
	peaks_frame.to_csv(outputfile,sep="\t",index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='find_peaks_human.py')
    parser.add_argument('-e','--expfile',help='Filename of experiment gnashyfile',required=True)
    parser.add_argument('-b','--bgfile',help='Filename of background gnashyfile',required=True)
    parser.add_argument('-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_peaks.txt')
    parser.add_argument('-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/TTAA_pos_hg19.txt')
    parser.add_argument('-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed')
    parser.add_argument('-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4)
    parser.add_argument('-pfc','--peakfinder_cutoff',help='peakfinder significance cutoff',required=False,default=1e-3)
    parser.add_argument('-w','--window_size',help='peakfinder window size',required=False,default=1000)
    parser.add_argument('-s','--step_size',help='peakfinder window size',required=False,default=500)
    parser.add_argument('-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2)
    parser.add_argument('-g','--genome',help='genome type, mm10 or hg19 are supported',required=False,default = "hg19")
    args = parser.parse_args()
    annotate_gnashyfile(args.expfile,args.bgfile,args.outputfile,args.TTAAfile,args.annotationfile,args.genome,
    	float(args.pvalue_cutoff),float(args.peakfinder_cutoff),int(args.window_size),int(args.step_size),float(args.pseudocounts))






