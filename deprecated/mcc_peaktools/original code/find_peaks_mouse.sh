#!/bin/bash
#SBATCH -n 8  
#SBATCH -N 1 
#SBATCH --mem=30000
#SBATCH -o find_peaks.out#standard out goes here
#SBATCH -e find_peaks.err # standard error goes here
#SBATCH -J FPe

module load pandas
module load scipy
module load bx-python
module load pybedtools

python find_peaks_mouse.py -e "M6Brd4.txt.gnashy" -b "M6WT.txt.gnashy" -o "M6Brd4_annotated_peaks.txt"


