"""
find_clusters_humans.py
written July 2017 by RDM
    '-e','--expfile',help='Filename of experiment gnashyfile',required=True
    '-b','--bgfile',help='Filename of background gnashyfile',required=True
    '-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_clusters.txt'
    '-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt'
    '-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed'
    '-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4
    '-cs','--cluster_size',help='cluster size',required=False,default=1000
    '-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2
    '-ms','--macsstyle',help='macs style pvalue calculation, required=False,default = True'
    '-g','--genome',help='genome type, mm10 or hg19 are supported',required=False,default = "hg19"
This script identify significant TF binding sites from mammalian calling card data collected with a TF-piggyBac
transposase fusion.  It borrows from MACS (Zhang, Liu et al Genome Biol 2008), the most popular peak
caller for ChIP-Seq data, but makes some modifications specific for Calling Card data.

The workflow is as follows.  
1.  Find candidate clusters.  This script clusters calling card insertions using hierarchical clustering.  
    It uses euclidean distance as the metric and seeks to minimize the average pairwise distance of cluster members.
    The cluster_size parameter is used to define the different clusters.  Because hierarchical clustering is computationally 
    inefficient, it breaks the chromosomes up into different pieces and clusters the individual pieces.  To ensure that the 
    breakpoints don't break up potentia cluster, it searches for regions of the chromosome with a large distance between
    insertions. 

2.  Call significant clusters.  Clusters are assigned a p-value by testing the observed number of hops in the cluster against 
the null model, which is a Poisson distribution parameterized by the background hops in the region.  As in MACS, 4 parameters are 
calculated, Lambda_bg, Lambda_1k, Lambda_5k, Lambda_10k, by using the background hops that are under the cluster (bg), 
the background hops that are in a 1kb window around the cluster center (1k), or a 5kb window (5kb) or a 10kb window (10kb).  These values 
are normalized to the number of TTAAs in each window, and then the maximum of this value is used for the null model.  

3.  Annotate Clusters.  Clusters are annotated using bedtools to provide information about nearby genes. 

changes:

"""
import numpy as np 
import pandas as pd
import scipy.stats as scistat
from bx.intervals.intersection import Intersecter, Interval
import pybedtools.bedtool as bt 
import argparse

def compute_cumulative_poisson(exp_hops_region,bg_hops_region,total_exp_hops,total_bg_hops,pseudocounts):
    #usage
    #scistat.poisson.cdf(x,mu)
    #scales sample with more hops down to sample with less hops
    #tested 6/14/17
    if total_bg_hops >= total_exp_hops:
        return(1-scistat.poisson.cdf((exp_hops_region+pseudocounts),bg_hops_region * (float(total_exp_hops)/float(total_bg_hops)) + pseudocounts))
    else:
        return(1-scistat.poisson.cdf(((exp_hops_region *(float(total_bg_hops)/float(total_exp_hops)) )+pseudocounts),bg_hops_region + pseudocounts))

def cluster_gnashyframe(gnashyframe,distance_cutoff):
    """This function takes a gnashy dataframe as input and clusters the insertions
    by hierarchical clustering.  It uses euclidean distance as the metric and seeks to minimize
    the average pairwise distance of cluster members.  The distance_cutoff parameter is used to define
    the different clusters.  Because hierarchical clustering is computationally inefficient, it breaks
    the chromosomes up into different pieces and clusters the individual pieces.  To ensure that the breakpoints
    don't break up potentia cluster, it searches for regions of the chromosome with a large distance between
    insertions.  If it cannot find a distance that is greater than the distance cutoff, then it prints a 
    warning.  The program outputs a frame of clusters with the following columns: 
    ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]"""

    MAX_ARRAY_SIZE = 5000  #maximum number of insertions to cluster
    MIN_JUMP = 1000 #minimum number of insertions to cluster

    cluster_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"])

    grouped = gnashyframe.groupby(['Chr']) #group insertions by chromosome
    
    for chro in grouped.groups:
        print "Analyzing chromosome "+str(chro)
        full_hop_list = list(grouped.get_group(chro)['Pos'])
        #find gap to split chromosome for clustering
        next_index = 0
        while next_index < len(full_hop_list):
            if (len(full_hop_list)-next_index) < MAX_ARRAY_SIZE:
                hop_list = full_hop_list[next_index:len(full_hop_list)]
                hop_list = [[x] for x in hop_list]
                old_index = next_index
                next_index = len(full_hop_list)
            else:
                gap_list = full_hop_list[next_index+MIN_JUMP:next_index+MAX_ARRAY_SIZE]
                difference_list = [j-i for i, j in zip(gap_list[:-1], gap_list[1:])]
                max_dist = max(difference_list)
                gap_index = difference_list.index(max(difference_list))+1
                hop_list = full_hop_list[next_index:next_index+MIN_JUMP+gap_index]
                hop_list = [[x] for x in hop_list]
                old_index = next_index
                next_index = next_index+MIN_JUMP+gap_index
                if max_dist < distance_cutoff:
                    print "Warning. Chromosome "+str(chro)+" "+hop_list[0]+" jump distance of "+\
                    max_dist+" is less than distance_cutoff of "+distance_cutoff+"."
                #print old_index, next_index-1, max_dist
            #cluster split chromosome
            Z = linkage(hop_list,'average','euclidean')
            #find clusters
            clusters = fcluster(Z,distance_cutoff,criterion='distance')
            #record clusters
            pos_clus_frame = pd.DataFrame(columns=['Hop Position','Cluster'])
            pos_clus_frame['Hop Position'] = hop_list
            pos_clus_frame['Cluster'] = clusters
            grouped_hops = pos_clus_frame.groupby(['Cluster'])
            for cluster in grouped_hops.groups:
                hops_in_cluster = list(grouped_hops.get_group(cluster)['Hop Position'])
                chromosome = chro
                start = min(hops_in_cluster)[0]
                end = max(hops_in_cluster)[0]
                center = np.median(hops_in_cluster)
                experiment_hops = len(hops_in_cluster) 
                fraction_experiment = float(experiment_hops)/len(gnashyframe)
                cluster_frame = cluster_frame.append(pd.DataFrame({"Chr":[chromosome],"Start":[start],
                    "End":[end],"Center":[center],"Experiment Hops":[experiment_hops],
                    "Fraction Experiment":[fraction_experiment]}),ignore_index = True)
    #sort cluster frame by chr then position
    cluster_frame = cluster_frame.sort_values(["Chr","Start"])
    cluster_frame = cluster_frame[["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]]
    cluster_frame = cluster_frame.reset_index()
    cluster_frame[["Chr","Start","End","Center","Experiment Hops"]] = cluster_frame[["Chr","Start","End","Center","Experiment Hops"]].astype(int)
    del cluster_frame["index"]
    return cluster_frame

def add_pvalues_to_cluster_frame_macs(cluster_frame,background_gnashyframe,TTAA_frame,total_experiment_hops,pseudocounts = 0.2,macs_pvalue=True):
    background_gnashy_dict = {} 
    background_dict_of_trees = {}
    total_background_hops = len(background_gnashyframe)
    TTAA_frame_gbChr_dict = {} 
    TTAA_dict_of_trees = {}
    list_of_l_names = ["bg","1k","5k","10k"]


    #make interval tree for TTAAs
    for name,group in TTAA_frame.groupby('Chr'): 
        TTAA_frame_gbChr_dict[name] = group
        TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
        #initialize tree
        TTAA_dict_of_trees[name] = Intersecter() 
        #populate tree with position as interval
        for idx, row in TTAA_frame_gbChr_dict[name].iterrows(): 
            TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))

    #group by chromosome and populate interval tree with positions of background hops
    for name,group in background_gnashyframe.groupby('Chr'):
        background_gnashy_dict[name] = group
        background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
        #initialize tree
        background_dict_of_trees[name] = Intersecter() 
        #populate tree with position as interval
        for idx, row in background_gnashy_dict[name].iterrows():    
            background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

    #go through cluster frame and compute pvalues 
    num_bg_hops_list =[]
    frac_bg_list = []
    lambda_type_list =[]
    lambda_list = []
    pvalue_list = []
    for idx,row in cluster_frame.iterrows():
        #add number of background hops in cluster to frame
        bg_overlap = background_dict_of_trees[name].find(row["Start"],row["End"])
        num_bg_hops = len(bg_overlap)
        num_bg_hops_list.append(num_bg_hops)
        #add fraction background 
        frac_bg_list.append(float(num_bg_hops)/total_background_hops)       
        #find lambda and compute significance of cluster

        #replace num_exp hops with row["Experiment Hops"]

        if total_background_hops >= total_experiment_hops: #scale bg hops down
            #compute lambda bg
            num_TTAAs = len(TTAA_dict_of_trees[name].find(row["Start"],row["End"]))
            lambda_bg = ((num_bg_hops*(float(total_experiment_hops)/total_background_hops))/num_TTAAs) 

            #compute lambda 1k
            num_bg_hops_1k = len(background_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            lambda_1k = (num_bg_hops_1k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_1k,1))


            #compute lambda 5k
            num_bg_hops_5k = len(background_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            lambda_5k = (num_bg_hops_5k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_5k,1))


            #compute lambda 10k
            num_bg_hops_10k = len(background_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            lambda_10k = (num_bg_hops_10k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_10k,1))
            if macs_pvalue:
                lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])
                index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
                lambda_type_list.append(list_of_l_names[index])
            else:
                lambda_f = lambda_bg
                lambda_type_list.append(list_of_l_names[0])
            lambda_list.append(lambda_f)
            #compute pvalue and record it
            pvalue = 1-scistat.poisson.cdf((row["Experiment Hops"]+pseudocounts),lambda_f*num_TTAAs+pseudocounts)
            pvalue_list.append(pvalue)

        else: #scale experiment hops down
            #compute lambda bg
            num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
            lambda_bg = (float(num_bg_hops)/num_TTAAs) 

            #compute lambda 1k
            num_bg_hops_1k = len(background_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            lambda_1k = (float(num_bg_hops_1k)/(max(num_TTAAs_1k,1)))

            #compute lambda 5k
            num_bg_hops_5k = len(background_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            lambda_5k = (float(num_bg_hops_5k)/(max(num_TTAAs_5k,1)))

            #compute lambda 10k
            num_bg_hops_10k = len(background_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            lambda_10k = (float(num_bg_hops_10k)/(max(num_TTAAs_10k,1)))
            
            if macs_pvalue:
                lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])
                index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
                lambda_type_list.append(list_of_l_names[index])
            else:
                lambda_f = lambda_bg
                lambda_type_list.append(list_of_l_names[0])

            #compute pvalue and record it
            pvalue = 1-scistat.poisson.cdf(((float(total_background_hops)/row["Experiment Hops"])*num_exp_hops+pseudocounts),lambda_f*num_TTAAs+pseudocounts)
            pvalue_list.append(pvalue)

            #else do nothing.
                         
    #make frame from all of the lists 
    cluster_frame["Background Hops"] = num_bg_hops_list 
    cluster_frame["Fraction Background"] = frac_bg_list
    cluster_frame["Lambda Type"] = lambda_type_list
    cluster_frame["Lambda"] = lambda_list
    cluster_frame["Poisson pvalue"] = pvalue_list
    return cluster_frame

def cluster_frame_to_bed(cluster_frame,bedfilename,genome_type='hg19'):
    bed_frame = cluster_frame[["Chr","Start","End"]].copy()
    bed_frame.loc[:,"Start"] = bed_frame["Start"]-1 #start coords of bed files are 0 indexed while ends are 1 indexed
    if (genome_type == 'hg18') or (genome_type == 'hg19'):
        chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
    else:
        chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
    bed_frame.loc[:,"Chr"] = [chr_dict[x] for x in bed_frame.loc[:,"Chr"]]
    bed_frame.to_csv(bedfilename,sep = "\t",header = None,index=None)


#def merge_bedfile_and_cluster_frame():

def annotate_cluster_frame(cluster_frame,refGene_filename = '/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed',genome_type='hg19'):
    "This function annotates clusters using pybedtools"

    cluster_frame_to_bed(cluster_frame,"temp_clusters.bed",genome_type)
    clusters_bed = bt.BedTool('temp_clusters.bed')
    clusters_bed = clusters_bed.sort()
    #convert clusters frame to bed
    clusters_bed = clusters_bed.closest('/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',D="ref",t="first",k=2)
    clusters_bed.saveas('temp_clusters.bed')

    #read in gene_annotation_bedfilename
    temp_annotated_clusters = pd.read_csv("temp_clusters.bed",delimiter = "\t",header=None)
    temp_annotated_clusters.columns = ["Chr","Start","End","Feature Chr","Feature Start", "Feature End","Feature sName","Feature Name","Strand","Distance"]
    temp_annotated_clusters.loc[:,"Start"] = temp_annotated_clusters["Start"]+1 #convert start coords back to 1 indexed coordinates
    index_list = [(x,y,z) for x,y,z in zip(temp_annotated_clusters["Chr"],temp_annotated_clusters["Start"],temp_annotated_clusters["End"])]

    temp_annotated_clusters.index = pd.MultiIndex.from_tuples(index_list)
    
    if (genome_type == 'hg18') or (genome_type == 'hg19'):
        chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
    else:
        chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
    cluster_frame.loc[:,"Chr"] = [chr_dict[x] for x in cluster_frame.loc[:,"Chr"]]
    
    index_list = [(x,y,z) for x,y,z in zip(cluster_frame["Chr"],cluster_frame["Start"],cluster_frame["End"])]
    cluster_frame.index = pd.MultiIndex.from_tuples(index_list)
    
    temp_list = list(cluster_frame.columns)
    temp_list = temp_list+["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand","Feature 1 Distance", 
        "Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand","Feature 2 Distance"]
    cluster_frame = cluster_frame.reindex(columns=temp_list,fill_value=0)

    for idx,row in temp_annotated_clusters.iterrows():
        if not(cluster_frame.loc[idx,"Feature 1 sName"]):
            cluster_frame.loc[idx,["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand",
            "Feature 1 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]]) 
        else:
            cluster_frame.loc[idx,["Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand",
            "Feature 2 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]])
    return cluster_frame      
    

def annotate_gnashyfile(expfile,bgfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/human/TTAA_pos_hg19.txt',
    annotation_file = '/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed',genome='hg19',pvalue_cutoff=1e-4,
    cluster_size = 1000,pseudocounts = 0.2,macsstyle = True):

    """This function is the wrapper that calls the various subfunctions"""

    expframe = pd.read_csv(expfile,delimiter="\t",header=None)
    expframe.columns = ["Chr","Pos","Reads"]
    total_experiment_hops = len(expframe)

    bgframe = pd.read_csv(bgfile,delimiter="\t",header=None)
    bgframe.columns = ["Chr","Pos","Reads"]

    TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
    TTAAframe.columns = ["Chr","Pos","Reads"]
    cluster_frame = cluster_gnashyframe(expframe,cluster_size)
    cluster_frame = add_pvalues_to_cluster_frame_macs(cluster_frame,bgframe,
        TTAAframe,total_experiment_hops,pseudocounts,macsstyle)
    cluster_frame = annotate_cluster_frame(cluster_frame,annotation_file,genome)
    cluster_frame = cluster_frame[cluster_frame["Poisson pvalue"] <= pvalue_cutoff]
    cluster_frame = cluster_frame.sort_values(["Poisson pvalue"])
    cluster_frame.to_csv(outputfile,sep="\t",index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='find_clusters_mouse.py')
    parser.add_argument('-e','--expfile',help='Filename of experiment gnashyfile',required=True)
    parser.add_argument('-b','--bgfile',help='Filename of background gnashyfile',required=True)
    parser.add_argument('-o','--outputfile',help='Filename of output file',required=False,default='annotated_cc_clusters.txt')
    parser.add_argument('-t','--TTAAfile',help='TTAA position file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/TTAA_pos_hg19.txt')
    parser.add_argument('-a','--annotationfile',help='Annotation file',required=False,default='/scratch/rmlab/ref/calling_card_ref/human/refGene.hg19.Sorted.bed')
    parser.add_argument('-pc','--pvalue_cutoff',help='significance cutoff',required=False,default=1e-4)
    parser.add_argument('-cs','--cluster_size',help='cluster size',required=False,default=1000)
    parser.add_argument('-ps','--pseudocounts',help='pseudocounts for poisson',required=False,default=0.2)
    parser.add_argument('-ms','--macsstyle',help='macs style pvalue calculation', required=False,default = True)
    parser.add_argument('-g','--genome',help='genome type, mm10 or hg19 are supported',required=False,default = "hg19")
    args = parser.parse_args()
    annotate_gnashyfile(args.expfile,args.bgfile,args.outputfile,args.TTAAfile,args.annotationfile,args.genome,
        float(args.pvalue_cutoff),float(args.cluster_size),float(args.pseudocounts),bool(args.macsstyle))


