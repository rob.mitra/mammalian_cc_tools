import numpy as np 
import pandas as pd
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import scipy.stats as scistat
from bx.intervals.intersection import Intersecter, Interval

#experiment_frame = pd.read_csv('M6Brd4.txt.gnashy', delimiter = "\t",header = None)
#experiment_frame.columns = ['Chr','Pos','Reads']

def cluster_gnashyframe(gnashyframe,distance_cutoff):
	"""This function takes a gnashy dataframe as input and clusters the insertions
	by hierarchical clustering.  It uses euclidean distance as the metric and seeks to minimize
	the average pairwise distance of cluster members.  The distance_cutoff parameter is used to define
	the different clusters.  Because hierarchical clustering is computationally inefficient, it breaks
	the chromosomes up into different pieces and clusters the individual pieces.  To ensure that the breakpoints
	don't break up potentia cluster, it searches for regions of the chromosome with a large distance between
	insertions.  If it cannot find a distance that is greater than the distance cutoff, then it prints a 
	warning.  The program outputs a frame of clusters with the following columns: 
	["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]"""

	MAX_ARRAY_SIZE = 5000  #maximum number of insertions to cluster
	MIN_JUMP = 1000 #minimum number of insertions to cluster

	cluster_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"])

	grouped = gnashyframe.groupby(['Chr']) #group insertions by chromosome
	
	for chro in grouped.groups:
		print "Analyzing chromosome "+str(chro)
		full_hop_list = list(grouped.get_group(chro)['Pos'])
		#find gap to split chromosome for clustering
		next_index = 0
		while next_index < len(full_hop_list):
			if (len(full_hop_list)-next_index) < MAX_ARRAY_SIZE:
				hop_list = full_hop_list[next_index:len(full_hop_list)]
				hop_list = [[x] for x in hop_list]
				old_index = next_index
				next_index = len(full_hop_list)
			else:
				gap_list = full_hop_list[next_index+MIN_JUMP:next_index+MAX_ARRAY_SIZE]
				difference_list = [j-i for i, j in zip(gap_list[:-1], gap_list[1:])]
				max_dist = max(difference_list)
				gap_index = difference_list.index(max(difference_list))+1
				hop_list = full_hop_list[next_index:next_index+MIN_JUMP+gap_index]
				hop_list = [[x] for x in hop_list]
				old_index = next_index
				next_index = next_index+MIN_JUMP+gap_index
				if max_dist < distance_cutoff:
					print "Warning. Chromosome "+str(chro)+" "+hop_list[0]+" jump distance of "+\
					max_dist+" is less than distance_cutoff of "+distance_cutoff+"."
				#print old_index, next_index-1, max_dist
			#cluster split chromosome
			Z = linkage(hop_list,'average','euclidean')
			#find clusters
			clusters = fcluster(Z,distance_cutoff,criterion='distance')
			#record clusters
			pos_clus_frame = pd.DataFrame(columns=['Hop Position','Cluster'])
			pos_clus_frame['Hop Position'] = hop_list
			pos_clus_frame['Cluster'] = clusters
			grouped_hops = pos_clus_frame.groupby(['Cluster'])
			for cluster in grouped_hops.groups:
				hops_in_cluster = list(grouped_hops.get_group(cluster)['Hop Position'])
				chromosome = chro
				start = min(hops_in_cluster)[0]
				end = max(hops_in_cluster)[0]
				center = np.median(hops_in_cluster)
				experiment_hops = len(hops_in_cluster) 
				fraction_experiment = float(experiment_hops)/len(gnashyframe)
				cluster_frame = cluster_frame.append(pd.DataFrame({"Chr":[chromosome],"Start":[start],
					"End":[end],"Center":[center],"Experiment Hops":[experiment_hops],
					"Fraction Experiment":[fraction_experiment]}),ignore_index = True)
	#sort cluster frame by chr then position
	cluster_frame = cluster_frame.sort_values(["Chr","Start"])
	cluster_frame = cluster_frame[["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]]
	cluster_frame = cluster_frame.reset_index()
	cluster_frame[["Chr","Start","End","Center","Experiment Hops"]] = cluster_frame[["Chr","Start","End","Center","Experiment Hops"]].astype(int)
	del cluster_frame["index"]
	return cluster_frame

def compute_cumulative_poisson(exp_hops_region,bg_hops_region,total_exp_hops,total_bg_hops,pseudocounts):
	#usage
	#scistat.poisson.cdf(x,mu)
	#scales sample with more hops down to sample with less hops
	#tested 6/14/17
	if total_bg_hops >= total_exp_hops:
		return(1-scistat.poisson.cdf((exp_hops_region+pseudocounts),bg_hops_region * (float(total_exp_hops)/float(total_bg_hops)) + pseudocounts))
	else:
		return(1-scistat.poisson.cdf(((exp_hops_region *(float(total_bg_hops)/float(total_exp_hops)) )+pseudocounts),bg_hops_region + pseudocounts))

def add_bg_hops_to_cluster_frame(cluster_frame,background_gnashyframe):
	"""This function takes in a cluster frame and a background_gnashyframe.  It
	builts and interval tree from all of the background hops.  It then walks through the
	cluster frame and finds the number of background hops between each cluster boundary.
	It adds this information to the cluster frame.  It also computes the fraction of background
	hops in the cluster boundaries and returns this information"""
	background_gnashy_dict = {} 
	background_dict_of_trees = {}
	total_background_hops = len(background_gnashyframe)
	#group by chromosome and populate interval tree with positions of background hops
	for name,group in background_gnashyframe.groupby('Chr'):
		background_gnashy_dict[name] = group
		background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
		#initialize tree
		background_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in background_gnashy_dict[name].iterrows():	
			background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero
	background_hops = []
	fraction_background = []	
	for idx,row in cluster_frame.iterrows():
		chr_name =  row["Chr"]
		overlap = background_dict_of_trees[chr_name].find(int(row['Start'])-4,int(row['End'])+4) #to account for insetions in opposite orientation
		background_hops.append(len(overlap))
		fraction_background.append(float(len(overlap))/total_background_hops)
	cluster_frame["Background Hops"] = background_hops
	cluster_frame["Fraction Background"] = fraction_background
	return cluster_frame

def add_poisson_pvalue_to_cluster_frame(cluster_frame,total_exp_hops,total_bg_hops,pseudocounts):
	poisson_pvalue = []
	for idx,row in cluster_frame.iterrows():
		poisson_pvalue.append(compute_cumulative_poisson(row["Experiment Hops"],
			row["Background Hops"],total_exp_hops,total_bg_hops,pseudocounts))
	cluster_frame["Poisson pvalue"] = poisson_pvalue
	return cluster_frame

def find_peaks_pvalue_method(experiment_gnashyframe,background_gnashyframe,pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500, distance_from_center = 1000,pseudocounts = 0.3):
	"""This program is passed a experiment frame (gnashy format) and 
	a background frame (gnashy format).  It then builds interval trees containing
	all of the background and experiment hops.  Next, it scans through the genome
	with a window of window_size and step size of step_size and looks for regions that
	have signficantly more experimental hops than background hops (poisson w/ pvalue_cutoff).
	It merges consecutively enriched windows and computes the center of the peak.  Next 
	it searches in a window centered at the peak center and checks for significance. Finally,
	it returns a frame that has Chr,Start,End,Center,Experiment Hops,
	Fraction Experiment,Background Hops,Fraction Background,Poisson pvalue

	"""
	peaks_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops",
		"Fraction Experiment","Background Hops","Fraction Background","Poisson pvalue"])

	
	experiment_gnashy_dict = {}
	experiment_dict_of_trees = {}
	total_experiment_hops = len(experiment_gnashyframe)
	background_gnashy_dict = {} 
	background_dict_of_trees = {}
	total_background_hops = len(background_gnashyframe)

	#group by chromosome and populate interval tree with positions of background hops
	for name,group in background_gnashyframe.groupby('Chr'):
		background_gnashy_dict[name] = group
		background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
		#initialize tree
		background_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in background_gnashy_dict[name].iterrows():	
			background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#group by chromosome and populate interval tree with positions of experiment hops
	for name,group in experiment_gnashyframe.groupby('Chr'):
		experiment_gnashy_dict[name] = group
		experiment_gnashy_dict[name].index = experiment_gnashy_dict[name]["Pos"]
		#initialize tree
		experiment_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in experiment_gnashy_dict[name].iterrows():	
			experiment_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#these will eventually be the columns in the peaks frame that will be returned.
	chr_list = []
	start_list = []
	end_list = []
	center_list = []
	num_exp_hops_list = []
	num_bg_hops_list = []
	frac_exp_list = []
	frac_bg_list = []
	pvalue_list = []
	
	#group experiment gnashyfile by chomosome
	for name,group in experiment_gnashyframe.groupby('Chr'):
		max_pos = max(group["Pos"])
		sig_start = 0
		sig_end = 0
		sig_flag = 0
		for window_start in range(1,max_pos+window_size,step_size):
			overlap = experiment_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_exp_hops = len(overlap)
			bg_overlap = background_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_bg_hops = len(bg_overlap)

			#is this window significant?
			if compute_cumulative_poisson(num_exp_hops,num_bg_hops,total_experiment_hops,total_background_hops,pseudocounts) < pvalue_cutoff:
				#was last window significant?
				if sig_flag:
					#if so, extend end of windows
					sig_end = window_start+window_size-1
				else:
					#otherwise, define new start and end and set flag
					sig_start = window_start
					sig_end = window_start+window_size-1
					sig_flag = 1
				if num_exp_hops >= 1:
					print
					print "current start = "+str(window_start)
					print "current end = "+str(window_start+window_size-1)
					print "sig start = " + str(sig_start)
					print "sig_end = " + str(sig_end)
					print "num exp hops = " + str(num_exp_hops)
					print "num bg hops = " + str(num_bg_hops)
					print "pvalue =" + str(compute_cumulative_poisson(num_exp_hops,num_bg_hops,total_experiment_hops,total_background_hops,pseudocounts))

			else:
				#current window not significant.  Was last window significant?
				if sig_flag:
					#add last window to cluster list, compute center, compute poisson from center
					chr_list.append(name)
					start_list.append(sig_start)
					end_list.append(sig_end)
					print "sig peak:"
					print"sig_start = "+str(sig_start)
					print "sig_end = "+str(sig_end)
					overlap = experiment_dict_of_trees[name].find(sig_start,sig_end)
					exp_hop_pos_list = [x.start for x in overlap]
					peak_center = np.median(exp_hop_pos_list)
					center_list.append(peak_center)
					#number of hops that are a user-defined distance from peak center
					print "peak_center = "+str(peak_center)
					print "center window start = "+str(peak_center-distance_from_center+1)
					print "center window end = "+str(peak_center+distance_from_center)
					#find number of hops from peak center in window, note this is not the same as the start and stop
					#of the peak, but instead a window around the center of the peak.
					print experiment_dict_of_trees[name].find(peak_center-distance_from_center+1,peak_center+distance_from_center)
					overlap = experiment_dict_of_trees[name].find(peak_center - distance_from_center+ 1,peak_center + distance_from_center)
					print "number of exp hops near center  = " + str(len(overlap))
					exp_hops_near_center = len(overlap)
					bg_overlap = background_dict_of_trees[name].find(peak_center - distance_from_center+ 1,peak_center + distance_from_center)
					bg_hops_near_center = len(bg_overlap)
					print "number of bg hops near center = " + str(bg_hops_near_center)
					num_bg_hops_list.append(bg_hops_near_center)
					num_exp_hops_list.append(exp_hops_near_center)
					frac_exp_list.append(float(exp_hops_near_center)/total_experiment_hops)
					frac_bg_list.append(float(bg_hops_near_center)/total_background_hops)
					pvalue_list.append(compute_cumulative_poisson(exp_hops_near_center,bg_hops_near_center,total_experiment_hops,total_background_hops,pseudocounts))
					#reset flag
					sig_flag = 0
				#else do nothing.
				if num_exp_hops >= 1:
					print
					print "current start = "+str(window_start)
					print "current end = "+str(window_start+window_size-1)
					print "sig start = " + str(sig_start)
					print "sig_end = " + str(sig_end)
					print "num exp hops = " + str(num_exp_hops)
					print "num bg hops = " + str(num_bg_hops)
					print "pvalue =" + str(compute_cumulative_poisson(num_exp_hops,num_bg_hops,total_experiment_hops,total_background_hops,pseudocounts))

				
	#make frame from all of the lists
	peaks_frame["Chr"] = chr_list
	peaks_frame["Start"] = start_list
	peaks_frame["End"] = end_list
	peaks_frame["Center"] = center_list
	peaks_frame["Experiment Hops"] = num_exp_hops_list 
	peaks_frame["Fraction Experiment"] = frac_exp_list 
	peaks_frame["Background Hops"] = num_bg_hops_list 
	peaks_frame["Fraction Background"] = frac_bg_list
	peaks_frame["Poisson pvalue"] = pvalue_list
	return peaks_frame
	
#compute lambda local as max lambda of 1,2,5,10 from 
#background poisson pvalue from 1,2,5,10  is this right?
#add a fold change cutoff  default = 1




#function: annotate_pvalue




