#!/bin/bash
#SBATCH -n 8  
#SBATCH -N 1 
#SBATCH --mem=30000
#SBATCH -o run_cc_filter_reads.out#standard out goes here
#SBATCH -e run_cc_filter_reads.err # standard error goes here
#SBATCH -J CFR

module load biopython


python cc_filter_reads.py -r1 Brd4_mesGBM_R1.fastq -r2 Brd4_mesGBM_R2.fastq \
	-i Brd4_mesGBM_I1.fastq -b barcode_file --hammp 1 --hammt 1 -o ./

