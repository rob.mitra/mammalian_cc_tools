"""
cc_filter_reads_single.py
written 8/18/16 by RDM
modified from cc_filter_reads

modified 9/7/16 by RDM to close filehandles (but not yet tested)


usage 
python cc_filter_reads.py -r1 <read1 file> 
-i <index file> -b<barcode file> -o <output path>
--hammt <hamming distance for transposon barcode>

required fields:
    -r1 read 1 filename (full path)
    -i index filename (full path)

not required
    -b barcode file = ../raw/barcodes.txt
    -o output path = ../output_and_analysis
    -tp 0
"""
import argparse
import csv
from Bio import SeqIO
from Bio import Seq
import os

def read_barcode_file(barcode_filename):
    #This function reads in the experiment name and the transposon barcodes
    #from a file which is in the following format:
    #expt name \t transposon barcode 1,transposon barcode 2, transposon barcode 3 etc.
    #It then returns a dictionary with key = transposon barcode, value = expt_name
    #The last line of the file should not have a return character
    reader = csv.reader(open(barcode_filename, 'r'),delimiter = '\t')
    d = {}
    for row in reader:
        exname,b2 = row
        for transposon_barcode in b2.split(","):
            d[transposon_barcode]=exname
    return d

def hamming_distance(s1, s2):
#Return the Hamming distance between equal-length sequences
    if len(s1) != len(s2):
        raise ValueError("Undefined for sequences of unequal length")
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

def filter_reads(read1fn,indexfn,barcodefn,outpath,hammt):
    #This function does the following:
    #1.  Reads barcodes and corresponding experiments into a dictionary
    #2.  Opens index and checks for transposon sequence
    #3.  If filter is passed, it prints the reads to a file of the format:
    #exptname_transposonbc_R1.fasta
    #4.  It also prints a master file of all of the reads file
    #5.  The program then outputs a brief QC that lists the fraction of reads that have a 
    #transposon barcode and the number of reads for each experiment and the total number of reads analyzed.
    #Define some infrequently changed variables
    
    
    #MATCH BARCODES TO EXPERIMENT
    BARCODE_LEN = 10
    barcode_dict = read_barcode_file(barcodefn)
    print "I have read in the experiment barcodes."
    
    #Put all filenames in this file for later
    filelist_filehandle = open(outpath+"cc_filelist.txt",'w')


    #Make  a dictionary of filehandles for each transposon barcode and undetermined barcodes
    
    r1_bc_filehandle_dict = {} #dictionary that contains a handle for each transposon barcode
    
    for key in barcode_dict.keys():
        r1_filename = outpath+barcode_dict[key]+"_"+key+"_R1.fastq"
        #print the filename minus the _R1.fasta suffix 
        #This makes the next step easier
        print >> filelist_filehandle,outpath+barcode_dict[key]+"_"+key
        r1_bc_filehandle_dict[key] = open(r1_filename,'w')
    
    filelist_filehandle.close()
       
    #Make a filehandle to dump undetermined reads
    r1Undet_filehandle = open(outpath+"undetermined_R1.fastq",'w')
    i1Undet_filehandle = open(outpath+"undetermined_I1.fastq",'w')
    #Make a filehandle for the QC information
    qc_filehandle = open(outpath+"qc_filter_reads.txt",'w')

    #get handles for read files
    r1Handle = open(read1fn,"rU") #open read1 file
    i1Handle = open(indexfn,"rU") #open index file
    
    #make iterators for index read and r2
    index_record_iter = SeqIO.parse(i1Handle,"fastq")
    #initialize QC counters
    total_reads = 0 #obvious
    matched_reads = 0 #how many reads have a transposon barcode
    expt_dict = {}  #how many reads per experiment

    for expt in barcode_dict.values(): #initialize expt_dict
        expt_dict[expt]=0

    #LOOP THROUGH READS
    for read1_record in SeqIO.parse(r1Handle,"fastq"):
        index_record = next(index_record_iter)
        total_reads += 1  # advance reads counter
        transbc = str(index_record.seq[0:BARCODE_LEN])
        #try to correct barcodes if the hamming distance cutoff is g.t. zero
        if (hammt > 0):
            for key in barcode_dict.keys():
                if hamming_distance(transbc,key[1])<= hammt:
                    transbc = key[1]
        #print transbc
        #is transposon barcode pair in dictionary?   
        if transbc in barcode_dict:
            #if so, increment matched reads
            matched_reads += 1
            #update reads for the experiment
            expt_name = barcode_dict[transbc]
            expt_dict[expt_name] += 1
            #output reads to the correct place
            print >> r1_bc_filehandle_dict[transbc],read1_record.format("fastq")
        else: #if there is no match, print reads to undetermined file
            print >> r1Undet_filehandle,read1_record.format("fastq")
            print >> i1Undet_filehandle,index_record.format("fastq")
    #print QC values to file
    print >> qc_filehandle,"There were "+str(total_reads)+" total reads"
    print >> qc_filehandle,str(matched_reads/float(total_reads))+" of the total reads also had matched barcodes."
    for key in expt_dict.keys():
        print >> qc_filehandle, str(key)+"\t"+str(expt_dict[key])
    qc_filehandle.close()

    #Close all filehandles
    i1Handle.close()
    r1Handle.close()
    i1Undet_filehandle.close() 
    r1Undet_filehandle.close()
    for key in r1_bcp_filehandle_dict.keys():
        r1_bcp_filehandle_dict[key].close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='cc_filter_reads.py')
    parser.add_argument('-r1','--read1',help='Read 1 filename (full path)',required=True)
    parser.add_argument('-i','--indexfn',help='index filename (full path)',required=True)
    parser.add_argument('-o','--outputpath',help='output path',required=False,default='../output_and_analysis/')
    parser.add_argument('-b','--barcodefile',help='barcode filename (full path)',required=False,default='../raw/barcodes.txt')
    parser.add_argument('--hammt',help='Transposon barcode hamming distance',required=False,default=0)
    args = parser.parse_args()
    if not args.outputpath[-1] == "/":
        args.outputpath = args.outputpath+"/"
    os.chdir(args.outputpath)
    filter_reads(args.read1,args.indexfn,args.barcodefile,args.outputpath,int(args.hammt))

    

