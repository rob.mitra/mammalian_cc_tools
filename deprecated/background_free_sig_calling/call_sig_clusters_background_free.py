"""
call_sig_clusters_background_free.py
written 12/6/16 by RDM
This script takes an annotated Calling Card cluster file and a TTAA weight file.
It then computes the significance of each cluster and makes a new column, saving
the output to the same file as the input.

How is significance called?
The program looks at all TTAAs in a cluster and adds up the weights.  It then computes the 
probablity of the observed number of hops (or more) in TTAAs within the cluster using the cumulative 
binomial distribution.

Usage

python call_sig_clusters_background_free.py -i <input cluster file> -t <TTAA weight file>
"""
from bx.intervals.intersection import Intersecter, Interval
import pandas as pd 
import argparse
import re

def call_sig_clusters(input,TTAAfile):
	#read in TTAA file and make dictionary
	#meanwhile make a tree with the start and stop coordinates of each
	dump_filehandle = open("treedump.txt",'w')
	#read TTAA weight file into a dataframe
	TTAA_frame = pd.read_csv(TTAAfile,delimiter = "\t",header = None,names = ["chr","pos","weight"],dtype={"chr": str})
	TTAA_frame_gbChr_dict = {} 
	TTAA_dict_of_trees = {}
	#group by chromosome and populate interval tree with TTAA positions
	for name,group in TTAA_frame.groupby('chr'):
		name = str(name)
		print name
		TTAA_frame_gbChr_dict[name] = group
		TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["pos"]
		#initialize tree
		TTAA_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in TTAA_frame_gbChr_dict[name].iterrows():	
			TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
			print >> dump_filehandle,name+"\t"+str(idx)+"\t"+str(idx+4)
	#read in input file
	input_frame = pd.read_csv(input,delimiter = "\t")
	for idx,row in input_frame.iterrows():
		overlap = ""
		match = re.search('chr(\S+)' ,row['Chromosome'])
		chr_name =  match.group(1)
		overlap = TTAA_dict_of_trees[chr_name].find(int(row['Start']),int(row['End'])+1)
		out = [ (x.start, x.end) for x in overlap ]
		print "chr: "+chr_name
		print row['Start']
		print row['End']+1
		print out
	return TTAA_dict_of_trees


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='cc_filter_reads.py')
    parser.add_argument('-i','--input',help='input cluster file',required=True)
    parser.add_argument('-t','--TTAAfile',help='TTAA weight file',required=True)
    args = parser.parse_args()
    call_sig_clusters(args.input,args.TTAAfile)