#This module contains functions for splitting, mapping, and 
#making ccf files from mammalian calling card reads.
#
#Written 7/25/2015 by RDM
#
#Modified:
#
#
from Bio import SeqIO
from Bio import Seq
import os
import pysam
import pandas as pd
import csv
import re

################
#Main Functions#
################

###################################
#Splitting and Filtering Functions#
###################################

def filter_reads_V2_transposon_index(read1fn,indexfn,read2fn=False,barcodefn='../raw/barcodes.txt',outpath='../output_and_analysis/',hammp=0,hammt=0,primer3prime = "GCGTCAATTTTACGCAGACTATCTTTCTAGGG"):

	#This function analyzes reads collected with the
	# V2 transposon index protocol.  See the V2 Transposon Index
	#document for more details.

	#This protocol collects data as follows: 

	#Read 1 has a 3-5 bp primer barcode followed by
	#a sequence from the 5' TR of the piggyBac transposon.  This sequence is 
	#typically TTTACGCAGACTATCTTTCTAGGG or GCGTCAATTTTACGCAGACTATCTTTCTAGGG.
	# Then comes the TTAA and then genomic 
	#sequence.  (The TTAA is also in the genome)

	#Read 2 reads through the restriction enzyme bank (at most 11bp) and then into
	#the genome at the paired site

	#The Index read reads into the transposon barcode

	#Read 2 is not required.  If False is passed then all is good.

	#This function does the following
	#1.  Reads barcodes and corresponding experiments into a dictionary
	#2.  Opens the read 1 and checks for transposon sequence
	#3.  If the tranposon sequence is present, it checks to see if the primer
	#barcode matches the transposon barcode
	#4.  If both filters are passed, it prints the reads to a file of the format:
	#exptname_primerbc_transposonbc_R1.fasta (or R2 or I2)
	#5.  It also prints a master file of all of the reads file
	#6.  The program then outputs a brief QC that lists the fraction of reads that have a 
	#transposon match, the fraction of reads that have matching primer and transposon barcodes
	#and the number of reads for each experiment and the total number of reads analyzed.

	#Define some infrequently changed variables

	#I need to change to derive tranposon barcode length and primer barcode length
	#from barcode file

	FILTER_SEQUENCE_LEN = 15
	FILTER_SEQUENCE = primer3prime[-1*FILTER_SEQUENCE_LEN:]
	PRIMER3PRIME_LEN = len(primer3prime)

	
	#MATCH BARCODES TO EXPERIMENT
	(barcode_dict,pbc_len,tbc_len) = read_barcode_file_w_len(barcodefn)
	print "I have read in the experiment barcodes."
	print "primer barcode len = "+str(pbc_len)
	print "transposon barcode len = "+str(tbc_len)

	PRIMER_BARCODE_START = 0  
	PRIMER_BARCODE_END = pbc_len-1
	TRANSPOSON_BARCODE_START = 0
	TRANSPOSON_BARCODE_END = tbc_len-1 

	#Put all filenames in this file for later
	filelist_filehandle = open(outpath+"cc_filelist.txt",'w')


	#Make  a dictionary of filehandles for each barcode pair and undetermined barcodes
	
	r1_bcp_filehandle_dict = {} #dictionary that contains the handles for each barcode pair
	if read2fn:
		r2_bcp_filehandle_dict = {} #same as above, but for read 2

	for key in barcode_dict.keys():
		r1_filename = outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]+"_R1.fastq"
		#print the filename minus the _R1.fasta suffix 
		#This makes the next step easier
		print >> filelist_filehandle,outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]
		r1_bcp_filehandle_dict[key] = open(r1_filename,'w')
		if read2fn:
			r2_filename = outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]+"_R2.fastq"
			r2_bcp_filehandle_dict[key]=open(r2_filename,'w')
	filelist_filehandle.close()

	#Make a filehandle to dump undetermined reads
	r1Undet_filehandle = open(outpath+"undetermined_R1.fastq",'w')
	i1Undet_filehandle = open(outpath+"undetermined_I1.fastq",'w')
	if read2fn:
		r2Undet_filehandle = open(outpath+"undetermined_R2.fastq",'w')
	
	#Make a filehandle for the QC information
	qc_filehandle = open(outpath+"qc_filter_reads.txt",'w')

	#get handles for read files
	r1Handle = open(read1fn,"rU") #open read1 file
	if read2fn:
		r2Handle = open(read2fn,"rU") #open read2 file
	i1Handle = open(indexfn,"rU") #open index file
	
	#make iterators for index read and r2
	if read2fn:
		read2_record_iter = SeqIO.parse(r2Handle,"fastq")
	
	index_record_iter = SeqIO.parse(i1Handle,"fastq")
	
	#initialize QC counters
	total_reads = 0
	reads_with_transposon_seq = 0
	matched_reads = 0
	expt_dict = {}
	for expt in barcode_dict.values():
		expt_dict[expt]=0

	#LOOP THROUGH READS
	for read1_record in SeqIO.parse(r1Handle,"fastq"):
		if read2fn:
			read2_record = next(read2_record_iter)
		index_record = next(index_record_iter)
		total_reads += 1  # advance reads counter
		if FILTER_SEQUENCE in read1_record.seq[0:pbc_len+PRIMER3PRIME_LEN+5]: #the five is just for slop, which can be useful for barcodes with mismatched lengths
			reads_with_transposon_seq = reads_with_transposon_seq + 1
			primerbc = str(read1_record.seq[PRIMER_BARCODE_START:PRIMER_BARCODE_END+1]) 
			transbc = str(index_record.seq[TRANSPOSON_BARCODE_START:TRANSPOSON_BARCODE_END+1])
                        #try to correct barcodes if the hamming distance cutoff is g.t. zero
			if ((hammp > 0) or (hammt > 0)):
				for key in barcode_dict.keys():
					if hamming_distance(primerbc,key[0])<=hammp:
						if hamming_distance(transbc,key[1])<= hammt:
							primerbc = key[0]
							transbc = key[1]
			#print primerbc,transbc
			#is primer_bc transposon barcode pair in dictionary?   
			if (primerbc,transbc) in barcode_dict:
				#if so, increment matched reads
				matched_reads += 1
				#update reads for the experiment
				expt_name = barcode_dict[(primerbc,transbc)]
				expt_dict[expt_name] += 1
				#output reads to the correct place
				print >> r1_bcp_filehandle_dict[(primerbc,transbc)],read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
				if read2fn:
					print >> r2_bcp_filehandle_dict[(primerbc,transbc)],read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
			else: #if there is no match, print reads to undetermined file
				print >> r1Undet_filehandle,read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
				if read2fn:
					print >> r2Undet_filehandle,read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
				print >> i1Undet_filehandle,index_record.format("fastq")[0:len(index_record.format("fastq"))-1]
		else: #if there is no match, print reads to undetermined file
			print >> r1Undet_filehandle,read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
			if read2fn:
				print >> r2Undet_filehandle,read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
			print >> i1Undet_filehandle,index_record.format("fastq")[0:len(index_record.format("fastq"))-1]
	#print QC values to file

	print >> qc_filehandle,"There were "+str(total_reads)+" total reads"
	print >> qc_filehandle,str(reads_with_transposon_seq/float(total_reads))+" of the reads had a transposon sequence."
	print >> qc_filehandle,str(matched_reads/float(total_reads))+" of the total reads also had matched barcodes."
	for key in expt_dict.keys():
		print >> qc_filehandle, str(key)+"\t"+str(expt_dict[key])
	qc_filehandle.close()

	#Close all filehandles
	i1Handle.close()
	r1Handle.close()
	i1Undet_filehandle.close() 
	r1Undet_filehandle.close()
	for key in r1_bcp_filehandle_dict.keys():
		r1_bcp_filehandle_dict[key].close()
	if read2fn:
		r2Handle.close()
		r2Undet_filehandle.close()
		for key in r2_bcp_filehandle_dict.keys():
			r2_bcp_filehandle_dict[key].close()


def filter_reads_aav(read1fn,read2fn,barcodefn,outpath,hammp,hammt,primer3prime = "GCGTCAATTTTACGCAGACTATCTTTCTAGGG"):
	"""
	

	R1: 5bp primer barcode followed by GCGTCAATTTTACGCAGACTATCTTTCTAGGG   TTAA
	R2:  6bp transposon barcode followed by bank of TaqI,MspI, Cst6 restriction sites followed
 	by genomic sequence.  All samples should have the same group of transposon barcodes.  
 	These are a mix of 20 plasmids with different barcodes:"""

        FILTER_SEQUENCE_LEN = 15
	FILTER_SEQUENCE = primer3prime[-1*FILTER_SEQUENCE_LEN:]
	PRIMER3PRIME_LEN = len(primer3prime)

	
	#MATCH BARCODES TO EXPERIMENT
	(barcode_dict,pbc_len,tbc_len) = read_barcode_file_w_len(barcodefn)
	print "I have read in the experiment barcodes."
	print "primer barcode len = "+str(pbc_len)
	print "transposon barcode len = "+str(tbc_len)

	PRIMER_BARCODE_START = 0  
	PRIMER_BARCODE_END = pbc_len-1
	TRANSPOSON_BARCODE_START = 0
	TRANSPOSON_BARCODE_END = tbc_len-1 

	#Put all filenames in this file for later
	filelist_filehandle = open(outpath+"cc_filelist.txt",'w')


	#Make  a dictionary of filehandles for each barcode pair and undetermined barcodes
	
	r1_bcp_filehandle_dict = {} #dictionary that contains the handles for each barcode pair
	r2_bcp_filehandle_dict = {} #same as above, but for read 2

	for key in barcode_dict.keys():
		r1_filename = outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]+"_R1.fastq"
		#print the filename minus the _R1.fasta suffix 
		#This makes the next step easier
		print >> filelist_filehandle,outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]
		r1_bcp_filehandle_dict[key] = open(r1_filename,'w')
		r2_filename = outpath+barcode_dict[key]+"_"+key[0]+"_"+key[1]+"_R2.fastq"
		r2_bcp_filehandle_dict[key]=open(r2_filename,'w')
	filelist_filehandle.close()

	#Make a filehandle to dump undetermined reads
	r1Undet_filehandle = open(outpath+"undetermined_R1.fastq",'w')
	r2Undet_filehandle = open(outpath+"undetermined_R2.fastq",'w')
	
	#Make a filehandle for the QC information
	qc_filehandle = open(outpath+"qc_filter_reads.txt",'w')

	#get handles for read files
	r1Handle = open(read1fn,"rU") #open read1 file
	r2Handle = open(read2fn,"rU") #open read2 file
	
	#make iterators for r2
	read2_record_iter = SeqIO.parse(r2Handle,"fastq")
	
	#initialize QC counters
	total_reads = 0
	reads_with_transposon_seq = 0
	matched_reads = 0
	expt_dict = {}
	for expt in barcode_dict.values():
		expt_dict[expt]=0

	#LOOP THROUGH READS
	for read1_record in SeqIO.parse(r1Handle,"fastq"):
		read2_record = next(read2_record_iter)
		total_reads += 1  # advance reads counter
		if FILTER_SEQUENCE in read1_record.seq[0:pbc_len+PRIMER3PRIME_LEN+5]: #the five is just for slop, which can be useful for barcodes with mismatched lengths
			reads_with_transposon_seq = reads_with_transposon_seq + 1
			primerbc = str(read1_record.seq[PRIMER_BARCODE_START:PRIMER_BARCODE_END+1]) 
			transbc = str(read2_record.seq[TRANSPOSON_BARCODE_START:TRANSPOSON_BARCODE_END+1])
                        #try to correct barcodes if the hamming distance cutoff is g.t. zero
			if ((hammp > 0) or (hammt > 0)):
				for key in barcode_dict.keys():
					if hamming_distance(primerbc,key[0])<=hammp:
						if hamming_distance(transbc,key[1])<= hammt:
							primerbc = key[0]
							transbc = key[1]
			#print primerbc,transbc
			#is primer_bc transposon barcode pair in dictionary?   
			if (primerbc,transbc) in barcode_dict:
				#if so, increment matched reads
				matched_reads += 1
				#update reads for the experiment
				expt_name = barcode_dict[(primerbc,transbc)]
				expt_dict[expt_name] += 1
				#output reads to the correct place
				print >> r1_bcp_filehandle_dict[(primerbc,transbc)],read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
				
				print >> r2_bcp_filehandle_dict[(primerbc,transbc)],read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
			else: #if there is no match, print reads to undetermined file
				print >> r1Undet_filehandle,read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
				print >> r2Undet_filehandle,read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
				
		else: #if there is no match, print reads to undetermined file
			print >> r1Undet_filehandle,read1_record.format("fastq")[0:len(read1_record.format("fastq"))-1]
			print >> r2Undet_filehandle,read2_record.format("fastq")[0:len(read2_record.format("fastq"))-1]
			
	#print QC values to file

	print >> qc_filehandle,"There were "+str(total_reads)+" total reads"
	print >> qc_filehandle,str(reads_with_transposon_seq/float(total_reads))+" of the reads had a transposon sequence."
	print >> qc_filehandle,str(matched_reads/float(total_reads))+" of the total reads also had matched barcodes."
	for key in expt_dict.keys():
		print >> qc_filehandle, str(key)+"\t"+str(expt_dict[key])
	qc_filehandle.close()

	#Close all filehandles

	r1Handle.close()
	r1Undet_filehandle.close()
	for key in r1_bcp_filehandle_dict.keys():
		r1_bcp_filehandle_dict[key].close()

	r2Handle.close()
	r2Undet_filehandle.close()
	for key in r2_bcp_filehandle_dict.keys():
		r2_bcp_filehandle_dict[key].close()








###################
#Mapping Functions#
###################



def map_reads(basefilename,genome,paired=False,trim_five=37,trim_three=0,qcutoff=10,outpath="../output_and_analysis"):
    """
	map_reads.py
	written 6/30/16 by RDM
	
	updated 7/7/16 by RDM to account for new PB primers with longer
	priming sequence (default trim is now 37)
	
	usage
	map_reads <base file name>  <genome> <paired end flag> 
	<five primer bases to trim>  <three primer bases to trim>
	<quality filter> -o <output path>

	required
	<base file name> <genome> 

	not required
	<paired end flag> default = False 
	<five primer bases to trim> default =37  
	<three primer bases to trim> default = 0
	<quality filter>  default = 10
	<output path> default = ../output_and_analysis

	This program requires that the bowtie2 module is loaded and
	that the following environment variable is set:
    export BOWTIE2_INDEXES='/scratch/rmlab/ref/bowtie2_indexes/mm10'

	It also requires the samtools module is loaded.
    """
    outfilename = basefilename+".bam"
    outerrname = basefilename+".err"
    r1_filename = basefilename+"_R1.fastq"
    
    if paired:
        r2_filename = basefilename+"_R2.fastq"
        bowtie2_string = "bowtie2 -x "+genome+" -1 "+r1_filename+" -2 "+r2_filename+" --trim5 "+str(trim_five)+" --trim3 "+str(trim_three)+" 2> "+outerrname+" |samtools view -bS -q"+str(qcutoff)+" > "+outfilename    
    else:
        bowtie2_string = "bowtie2 -x "+genome+" -U "+r1_filename+" --trim5 "+str(trim_five)+" --trim3 "+str(trim_three)+" 2> "+outerrname+" |samtools view -bS -q"+str(qcutoff)+" > "+outfilename
    print bowtie2_string
    os.system(bowtie2_string)



###########################
#Functions to make ccffile#
###########################

def make_ccffile(genome="mm10",bcfilename="../raw/barcodes.txt",outpath="../output_and_analysis/"):
	"""	make_ccffile <bcfilename> <outpath>
		
		required
		none

		not required
		<genome>, default = "mm10"
		<bcfilename>, default = ../output_and_analysis/barcodes.txt  path and name of barcode file
		<outpath>, default = ../output_and_analysis/  path for input and output

		verified as OK 7/25/17 by RDM
	"""
	pattern = "^hg"
	if re.search(pattern,genome,re.I):
		chr_list = ['chr1','chr2','chr3','chr4','chr5',
		'chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chr20','chr21','chr22','chrX','chrY']
		print "making human ccffile"
	else:
		chr_list = ['chr1','chr2','chr3','chr4','chr5','chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chrX','chrY']
		print "making mouse ccffile"

	barcode_dict = read_barcode_file(bcfilename)
	#read experiments and barcodes into dictionary  
	#Key = (primer barcode, Transposon barode)
	#Value = expt name
	
	qc_dict = {}  #initialize quality control dictionary

	#LOOP THROUGH EXPERIMENTS
	#Loop through experiments and make a separate ccf file for each
	for expt in list(set(barcode_dict.values())):
		#for each experiment, there will be multiple bam files.  Loop through all of them
		#open output ccffile
		print "Analyzing "+expt
		output_filename = outpath+expt+".ccf"
		output_handle = file(output_filename, 'w') 
		#LOOP THROUGH BAM FILES CORRESPONDING TO 1 experiment
		for key in barcode_dict.keys(): #this could be made more efficient, but its more clear this way
			if barcode_dict[key] == expt:
				primerBC = key[0]
				transposonBC = key[1]
				basename = outpath+expt+"_"+primerBC+"_"+transposonBC
				sbamFilename = basename+".sorted.bam"
				pysam.sort("-o",sbamFilename,basename+".bam")
				#sort and index bamfile
				sbamFilename = sbamFilename
		   
				pysam.index(sbamFilename)
				print sbamFilename
				#inialize ccf dictionary
				ccf_dict = {}
				#make AlignmentFile object
                                current_bamfile = pysam.AlignmentFile(sbamFilename,"rb")

				#loop through the chromosomes and pileup start sites
				for chr in chr_list:
					aligned_reads_group = current_bamfile.fetch(chr)
					#now loop through each read and pile up start sites
					for aread in aligned_reads_group:
						#is the read a reverse read?
						if aread.is_reverse:
							#does it align to a ttaa?
							if (aread.get_reference_sequence()[-4:].lower()=='ttaa'):
								#if so, get position and update dictionary
								startpos = aread.get_reference_positions()[-1] + 1 - 3 #zero indexed, but I want 1 indexed for output
								endpos = startpos + 3
								strand = "-"
								if (chr,startpos,endpos,strand) in ccf_dict:
									ccf_dict[(chr,startpos,endpos,strand)]+=1
								else:
									ccf_dict[(chr,startpos,endpos,strand)]=1
						else: #forward read
							#does it align to a ttaa?
                                                        if (aread.get_reference_sequence()[0:4].lower()=='ttaa'):
								#if so, get position and update dicitonary
								startpos = aread.get_reference_positions()[0] + 1 #zero indexed, but I want 1 indexed for output
								endpos = startpos + 3
								strand = "+"
								if (chr,startpos,endpos,strand) in ccf_dict:
									ccf_dict[(chr,startpos,endpos,strand)]+=1
								else:
									ccf_dict[(chr,startpos,endpos,strand)]=1
				#we've gone through all reads on all chromsomes, so output the dictionary to ccf file
				for key in ccf_dict:
					output_handle.write("%s\t%s\t%s\t%s\t%s\t%s\n" %(key[0],key[1],key[2],ccf_dict[key],key[3],transposonBC))
		#we've gone through all barcodes in this experiment so close the output file
		output_handle.close()
		#OPEN ccf FILE AND SORT BY CHR THEN POS
		qc_dict[expt] = sort_ccf_file(output_filename,chr_list)
	#after all experiments have been analyzed, print out qc
	qc_handle = file(outpath+"ccfQC.txt",'w')
	for key in qc_dict:
		qc_handle.write("%s\t%s\t%s\n" %(key,qc_dict[key][0], qc_dict[key][1] ))
	qc_handle.close()


###################
#Helper Functions##
###################

def read_barcode_file(barcode_filename):
	#This helper function reads in the experiment name, the primer barcode, and the transposon barcodes
	#from a file which is in the following format:
	#expt name \t primer barcode \t transposon barcode 1,transposon barcode 2, transposon barcode 3 etc.
	#It then returns a dictionary with key = a tuple (primer barcode, transposon barcode), value = expt_name
	#The last line of the file should not have a return character
	reader = csv.reader(open(barcode_filename, 'r'),delimiter = '\t')
	d = {}
	for row in reader:
		exname,b1,b2 = row
		for transposon_barcode in b2.split(","):
			d[(b1,transposon_barcode)]=exname
	return d

def read_barcode_file_w_len(barcode_filename):
	#This helper function reads in the experiment name, the primer barcode, and the transposon barcodes
	#from a file which is in the following format:
	#expt name \t primer barcode \t transposon barcode 1,transposon barcode 2, transposon barcode 3 etc.
	#It then returns a dictionary with key = a tuple (primer barcode, transposon barcode), value = expt_name
	#The last line of the file should not have a return character
	#NOTE ALL BARCODES MUST BE OF THE SAME LENGTH!!
	reader = csv.reader(open(barcode_filename, 'r'),delimiter = '\t')
	d = {}
	for row in reader:
		exname,b1,b2 = row
		PBC_LEN = len(b1)
		for transposon_barcode in b2.split(","):
			TBC_LEN=len(transposon_barcode)
			d[(b1,transposon_barcode)]=exname
	return d,PBC_LEN,TBC_LEN

def hamming_distance(s1, s2):
#This helper function returns
#the Hamming distance between two equal-length sequences
	if len(s1) != len(s2):
		raise ValueError("Undefined for sequences of unequal length")
	return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

def sort_ccf_file(ccffilename,chrlist):
	ccf_frame = pd.read_csv(ccffilename,delimiter='\t',header=None,names=['Chr','Start','End','Reads','Strand','Barcode'])
	ccf_frame["Chr"] = pd.Categorical(ccf_frame["Chr"],chrlist)
	ccf_frame = ccf_frame.sort_values(['Chr','Start','End'])
	ccf_frame.to_csv(ccffilename,sep='\t',header=False,index=False)
	return [len(ccf_frame),ccf_frame.Reads.sum()]

######################
#Deprecated Functions#
######################

def map_reads_novo(basefilename,genome,paired=False,transposon_sequence="TTTACGCAGACTATCTTTCTAGGG",map_len=250,qcutoff=10,outpath="../output_and_analysis/"):
	#This function is very slow for mammalian genomes.  Use map reads.
	"""
	map_reads_novo.py
	written 3/24/16  Modified 7/25/17 by RDM for mammalian usage
	usage

	map_reads -f <base file name> -g <genome> -p <paired end flag> 
	-t transponson sequence to trim -l <length of sequence 3' to transposon
	to map> -q <quality filter> -o <output path>

	required
	-f <base file name> -g <genome> 

	not required
	-p <paired end flag> default = False 
	-t transposon sequence to trim default = TTTACGCAGACTATCTTTCTAGGG
	-l <length of sequence 3' to transposon to map> default = all
	-q <quality filter>  default = 250
	-o <output path> default = ../output_and_analysis

	This function requires that the novoalign module is loaded and
	It also requires the samtools module is loaded."""
	
	outfilename = basefilename+".bam"
	outerrname = basefilename+".err"
	r1_filename = basefilename+"_R1.fastq"
	regex = r"([A,C,G,T]+)_[A,C,G,T]+$"
	match = re.search(regex,basefilename)
	barcode_sequence = match.group(1)
	trim_seq = barcode_sequence+transposon_sequence
	map_len = int(map_len) + len(trim_seq)
	print "filtering "+trim_seq
	
	if paired:
		r2_filename = basefilename+"_R2.fastq"
		"""novoalign_string = ("novoalign -o SAM -d /scratch/rmlab/ref/novoalign_indexes/"+genome+".nix -f "+r1_filename+ 
		" "+r2_filename+" -5 "+trim_seq+" -n "+str(map_len)+" 2> "+outerrname+" |samtools view -bS -q"+str(qcutoff)+ 
		" > "+outfilename)""" #old command without explilcit paired end reads settings.
		novoalign_string = ("novoalign -o SAM -d /scratch/rmlab/ref/novoalign_indexes/"+genome+".nix -i PE 50-700 -f "
		+r1_filename+" "+r2_filename+" -5 "+trim_seq+" -n "+str(map_len)+" 2> "+outerrname+" |samtools view -bS -q"+str(qcutoff)+ 
		" > "+outfilename)    
	else:
		novoalign_string = ("novoalign -o SAM -d /scratch/rmlab/ref/novoalign_indexes/"+genome+".nix -f "+r1_filename+
		" -5 "+trim_seq+" -n "+str(map_len)+" 2> "+outerrname+" |samtools view -bS -q"+str(qcutoff)+" > "+outfilename)  
	print novoalign_string
	os.system(novoalign_string)

