#!/usr/bin/env python
import numpy as np
import pandas as pd
import scipy.stats as scistat
from bx.intervals.intersection import Intersecter, Interval
import pybedtools.bedtool as bt 
import argparse
from scipy.cluster.hierarchy import dendrogram,linkage,fcluster
import re


def call_peaks_and_annotate(expfile,bgfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt',
	annotation_file = '/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',genome='mm10',pvalue_cutoff=1e-4,
	peak_pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):

	"""This function is the wrapper that calls the various subfunctions"""

	expframe = pd.read_csv(expfile,delimiter="\t",header=None)
	expframe.columns = ["Chr","Pos","Reads"]

	bgframe = pd.read_csv(bgfile,delimiter="\t",header=None)
	bgframe.columns = ["Chr","Pos","Reads"]

	TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
	TTAAframe.columns = ["Chr","Pos","Reads"]

	peaks_frame = find_peaks(expframe,bgframe,TTAAframe,peak_pvalue_cutoff,window_size,step_size,pseudocounts)
	peaks_frame = annotate_peaks_frame(peaks_frame,annotation_file,genome)
	peaks_frame = peaks_frame[peaks_frame["Poisson pvalue"] <= pvalue_cutoff]
	peaks_frame = peaks_frame.sort_values(["Poisson pvalue"])
	peaks_frame.to_csv(outputfile,sep="\t",index=False)
    bed_frame = p_or_c_frame_to_bed(peaks_frame)
    pattern = "^(\S+)\."
    group = re.search(pattern,outputfile)
    bedfilename = group.groups(0)[0]+".bed"
    bed_frame.to_csv(bedfilename,sep="\t",index=False,header=False)



def call_peaks_and_annotate_bf(expfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt',
	annotation_file = '/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',genome='mm10',pvalue_cutoff=1e-4,
	peak_pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):

	"""This function is the wrapper that calls the various subfunctions"""

	expframe = pd.read_csv(expfile,delimiter="\t",header=None)
	expframe.columns = ["Chr","Pos","Reads"]


	TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
	TTAAframe.columns = ["Chr","Pos","Reads"]

        peaks_frame = find_peaks_bf(expframe,TTAAframe,peak_pvalue_cutoff,window_size,step_size,pseudocounts)
	peaks_frame = annotate_peaks_frame(peaks_frame,annotation_file,genome)
	peaks_frame = peaks_frame[peaks_frame["Poisson pvalue"] <= pvalue_cutoff]
	peaks_frame = peaks_frame.sort_values(["Poisson pvalue"])
	peaks_frame.to_csv(outputfile,sep="\t",index=False)
    bed_frame = p_or_c_frame_to_bed(peaks_frame)
    pattern = "^(\S+)\."
    group = re.search(pattern,outputfile)
    bedfilename = group.groups(0)[0]+".bed"
    bed_frame.to_csv(bedfilename,sep="\t",index=False,header=False)
    
def p_or_c_frame_to_bed(peaks_frame):
    """This function converts peaks from to a bed file that can be used to
    display the peak locations on the EPCC browser.  The minimum column 
    requirement for the peaks frame is [Chr,Start,End].  This function
    will return a bed frame with the following columns:  
    [Chr in mm10 or hg19 format,Start,End,name,score]"""
    
    bed_frame = peaks_frame[["Chr","Start","End"]].copy()
    name_list = list(peaks_frame["Poisson pvalue"].astype(str))
    score_list = [1000]*len(peaks_frame)
    bed_frame["Name"] = name_list
    bed_frame["Score"] = score_list
    return bed_frame

def cluster_and_annotate(expfile,bgfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt',
    annotation_file = '/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',genome='mm10',pvalue_cutoff=1e-4,
    cluster_size = 1000,pseudocounts = 0.2,macsstyle = True):

    """This function is the wrapper that calls the various subfunctions"""

    expframe = pd.read_csv(expfile,delimiter="\t",header=None)
    expframe.columns = ["Chr","Pos","Reads"]
    total_experiment_hops = len(expframe)

    bgframe = pd.read_csv(bgfile,delimiter="\t",header=None)
    bgframe.columns = ["Chr","Pos","Reads"]

    TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
    TTAAframe.columns = ["Chr","Pos","Reads"]
    cluster_frame = cluster_gnashyframe(expframe,cluster_size)
    cluster_frame = add_pvalues_to_cluster_frame_macs(cluster_frame,bgframe,
        TTAAframe,total_experiment_hops,pseudocounts,macsstyle)
    cluster_frame = annotate_peaks_frame(cluster_frame,annotation_file,genome)
    cluster_frame = cluster_frame[cluster_frame["Poisson pvalue"] <= pvalue_cutoff]
    cluster_frame = cluster_frame.sort_values(["Poisson pvalue"])
    cols = ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment","Background Hops","Fraction Background","Lamda Type",
            "Lambda","Poisson pvalue","Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand","Feature 1 Dist",
            "Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand","Feature 2 Dist"]
    cluster_frame = cluster_frame[cols] #reorder columns
    cluster_frame.to_csv(outputfile,sep="\t",index=False)
    bed_frame = p_or_c_frame_to_bed(peaks_frame)
    pattern = "^(\S+)\."
    group = re.search(pattern,outputfile)
    bedfilename = group.groups(0)[0]+".bed"
    bed_frame.to_csv(bedfilename,sep="\t",index=False,header=False)

def cluster_and_annotate_bf(expfile,bgfile,outputfile,TTAA_file = '/scratch/rmlab/ref/calling_card_ref/mouse/TTAA_mm10.txt',
    annotation_file = '/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',genome='mm10',pvalue_cutoff=1e-4,
    cluster_size = 1000,pseudocounts = 0.2,macsstyle = True):

    """This function is the wrapper that calls the various subfunctions"""

    expframe = pd.read_csv(expfile,delimiter="\t",header=None)
    expframe.columns = ["Chr","Pos","Reads"]
    total_experiment_hops = len(expframe)

    TTAAframe = pd.read_csv(TTAA_file,delimiter="\t",header=None)
    TTAAframe.columns = ["Chr","Pos","Reads"]
    cluster_frame = cluster_gnashyframe(expframe,cluster_size)
    cluster_frame = add_pvalues_to_cluster_frame_macs_bf(cluster_frame,
            expframe,TTAAframe,pseudocounts,macsstyle)
    cluster_frame = annotate_peaks_frame(cluster_frame,annotation_file,genome)
    cluster_frame = cluster_frame[cluster_frame["Poisson pvalue"] <= pvalue_cutoff]
    cluster_frame = cluster_frame.sort_values(["Poisson pvalue"])
    cols = ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment","Background Hops","Fraction Background","Lamda Type",
            "Lambda","Poisson pvalue","Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand","Feature 1 Dist",
            "Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand","Feature 2 Dist"]
    cluster_frame = cluster_frame[cols] #reorder columns
    cluster_frame.to_csv(outputfile,sep="\t",index=False)
    bed_frame = p_or_c_frame_to_bed(peaks_frame)
    pattern = "^(\S+)\."
    group = re.search(pattern,outputfile)
    bedfilename = group.groups(0)[0]+".bed"
    bed_frame.to_csv(bedfilename,sep="\t",index=False,header=False)

def cluster_gnashyframe(gnashyframe,distance_cutoff):
    """This function takes a gnashy dataframe as input and clusters the insertions
    by hierarchical clustering.  It uses euclidean distance as the metric and seeks to minimize
    the average pairwise distance of cluster members.  The distance_cutoff parameter is used to define
    the different clusters.  Because hierarchical clustering is computationally inefficient, it breaks
    the chromosomes up into different pieces and clusters the individual pieces.  To ensure that the breakpoints
    don't break up potentia cluster, it searches for regions of the chromosome with a large distance between
    insertions.  If it cannot find a distance that is greater than the distance cutoff, then it prints a 
    warning.  The program outputs a frame of clusters with the following columns: 
    ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]"""

    MAX_ARRAY_SIZE = 5000  #maximum number of insertions to cluster
    MIN_JUMP = 1000 #minimum number of insertions to cluster

    cluster_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"])

    grouped = gnashyframe.groupby(['Chr']) #group insertions by chromosome
    
    for chro in grouped.groups:
        print "Analyzing chromosome "+str(chro)
        full_hop_list = list(grouped.get_group(chro)['Pos'])
        #find gap to split chromosome for clustering
        next_index = 0
        while next_index < len(full_hop_list):
            if (len(full_hop_list)-next_index) < MAX_ARRAY_SIZE:
                hop_list = full_hop_list[next_index:len(full_hop_list)]
                hop_list = [[x] for x in hop_list]
                old_index = next_index
                next_index = len(full_hop_list)
            else:
                gap_list = full_hop_list[next_index+MIN_JUMP:next_index+MAX_ARRAY_SIZE]
                difference_list = [j-i for i, j in zip(gap_list[:-1], gap_list[1:])]
                max_dist = max(difference_list)
                gap_index = difference_list.index(max(difference_list))+1
                hop_list = full_hop_list[next_index:next_index+MIN_JUMP+gap_index]
                hop_list = [[x] for x in hop_list]
                old_index = next_index
                next_index = next_index+MIN_JUMP+gap_index
                if max_dist < distance_cutoff:
                    print "Warning. Chromosome "+str(chro)+" "+hop_list[0]+" jump distance of "+\
                    max_dist+" is less than distance_cutoff of "+distance_cutoff+"."
                #print old_index, next_index-1, max_dist
            #cluster split chromosome
            Z = linkage(hop_list,'average','euclidean')
            #find clusters
            clusters = fcluster(Z,distance_cutoff,criterion='distance')
            #record clusters
            pos_clus_frame = pd.DataFrame(columns=['Hop Position','Cluster'])
            pos_clus_frame['Hop Position'] = hop_list
            pos_clus_frame['Cluster'] = clusters
            grouped_hops = pos_clus_frame.groupby(['Cluster'])
            for cluster in grouped_hops.groups:
                hops_in_cluster = list(grouped_hops.get_group(cluster)['Hop Position'])
                chromosome = chro
                start = min(hops_in_cluster)[0]
                end = max(hops_in_cluster)[0]
                center = np.median(hops_in_cluster)
                experiment_hops = len(hops_in_cluster) 
                fraction_experiment = float(experiment_hops)/len(gnashyframe)
                cluster_frame = cluster_frame.append(pd.DataFrame({"Chr":[chromosome],"Start":[start],
                    "End":[end],"Center":[center],"Experiment Hops":[experiment_hops],
                    "Fraction Experiment":[fraction_experiment]}),ignore_index = True)
    #sort cluster frame by chr then position
    cluster_frame = cluster_frame.sort_values(["Chr","Start"])
    cluster_frame = cluster_frame[["Chr","Start","End","Center","Experiment Hops","Fraction Experiment"]]
    cluster_frame = cluster_frame.reset_index()
    cluster_frame[["Chr","Start","End","Center","Experiment Hops"]] = cluster_frame[["Chr","Start","End","Center","Experiment Hops"]].astype(int)
    del cluster_frame["index"]
    cluster_frame.to_csv("clustered_hops.txt",sep="\t",index=False)
    return cluster_frame

def find_peaks(experiment_gnashyframe,background_gnashyframe,TTAA_frame,pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):
	"""This program roughly follows the algorithm used by MACS to call
	ChIP-Seq peaks.  The function is passed a experiment frame (gnashy format), 
	a background frame (gnashy format), and an TTAA_frame.  It then builds interval trees containing
	all of the background and experiment hops and all of the TTAAs.  Next, it scans through the genome
	with a window of window_size and step size of step_size and looks for regions that
	have signficantly more experiment hops than background hops (poisson w/ pvalue_cutoff).
	It merges consecutively enriched windows and computes the center of the peak.  Next 
	it computes lambda, the number of insertions per TTAA expected from the background
	distribution by taking the max of lambda_bg, lamda_1, lamda_5, lambda_10.  It then computes
	a p-value based on the expected number of hops = lamda * number of TTAAs in peak * number of hops
	in peak.  Finally, it returns a frame that has Chr,Start,End,Center,Experiment Hops,
	Fraction Experiment,Background Hops,Fraction Background,Poisson pvalue

	"""
	peaks_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops",
		"Fraction Experiment","Background Hops","Fraction Background","Lambda Type",
		"Lambda","Poisson pvalue"])

	
	experiment_gnashy_dict = {}
	experiment_dict_of_trees = {}
	total_experiment_hops = len(experiment_gnashyframe)
	background_gnashy_dict = {} 
	background_dict_of_trees = {}
	total_background_hops = len(background_gnashyframe)
	TTAA_frame_gbChr_dict = {} 
	TTAA_dict_of_trees = {}
	list_of_l_names = ["bg","1k","5k","10k"]

	#group by chromosome and populate interval tree with TTAA positions
	for name,group in TTAA_frame.groupby('Chr'):
		 
		TTAA_frame_gbChr_dict[name] = group
		TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
		#initialize tree
		TTAA_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in TTAA_frame_gbChr_dict[name].iterrows():	
			TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
	#group by chromosome and populate interval tree with positions of background hops
	for name,group in background_gnashyframe.groupby('Chr'):
		background_gnashy_dict[name] = group
		background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
		#initialize tree
		background_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in background_gnashy_dict[name].iterrows():	
			background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#group by chromosome and populate interval tree with positions of experiment hops
	for name,group in experiment_gnashyframe.groupby('Chr'):
		experiment_gnashy_dict[name] = group
		experiment_gnashy_dict[name].index = experiment_gnashy_dict[name]["Pos"]
		#initialize tree
		experiment_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in experiment_gnashy_dict[name].iterrows():	
			experiment_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero

	#these will eventually be the columns in the peaks frame that will be returned.
	chr_list = []
	start_list = []
	end_list = []
	center_list = []
	num_exp_hops_list = []
	num_bg_hops_list = []
	frac_exp_list = []
	frac_bg_list = []
	lambda_type_list =[]
	lambda_list = []
	pvalue_list = []
	
	#group experiment gnashyfile by chomosome
	for name,group in experiment_gnashyframe.groupby('Chr'):
		max_pos = max(group["Pos"])
		sig_start = 0
		sig_end = 0
		sig_flag = 0
		for window_start in range(1,max_pos+window_size,step_size):
			overlap = experiment_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_exp_hops = len(overlap)
			bg_overlap = background_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_bg_hops = len(bg_overlap)

			#is this window significant?
			if compute_cumulative_poisson(num_exp_hops,num_bg_hops,total_experiment_hops,total_background_hops,pseudocounts) < pvalue_cutoff:
				#was last window significant?
				if sig_flag:
					#if so, extend end of windows
					sig_end = window_start+window_size-1
				else:
					#otherwise, define new start and end and set flag
					sig_start = window_start
					sig_end = window_start+window_size-1
					sig_flag = 1

			else:
				#current window not significant.  Was last window significant?
				if sig_flag:
					#add full sig window to the frame of peaks 
					
					#add chr, peak start, peak end
					chr_list.append(name) #add chr to frame
					start_list.append(sig_start) #add peak start to frame
					end_list.append(sig_end) #add peak end to frame
				
					#compute peak center and add to frame
					overlap = experiment_dict_of_trees[name].find(sig_start,sig_end)
					exp_hop_pos_list = [x.start for x in overlap]
					peak_center = np.median(exp_hop_pos_list)
					center_list.append(peak_center) #add peak center to frame

					#add number of experiment hops in peak to frame
					num_exp_hops = len(overlap)
					num_exp_hops_list.append(num_exp_hops)

					#add fraction of experiment hops in peak to frame
					frac_exp_list.append(float(num_exp_hops)/total_experiment_hops)

					#add number of background hops in peak to frame
					bg_overlap = background_dict_of_trees[name].find(sig_start,sig_end)
					num_bg_hops = len(bg_overlap)
					num_bg_hops_list.append(num_bg_hops)

					frac_bg_list.append(float(num_bg_hops)/total_background_hops)		
					#find lambda and compute significance of peak
					if total_background_hops >= total_experiment_hops: #scale bg hops down
						#compute lambda bg
						num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
						lambda_bg = ((num_bg_hops*(float(total_experiment_hops)/total_background_hops))/max(num_TTAAs,1)) 


						#compute lambda 1k
						num_bg_hops_1k = len(background_dict_of_trees[name].find(peak_center-499,peak_center+500))
						num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(peak_center-499,peak_center+500))
						lambda_1k = (num_bg_hops_1k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_1k,1))


						#compute lambda 5k
						num_bg_hops_5k = len(background_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						lambda_5k = (num_bg_hops_5k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_5k,1))


						#compute lambda 10k
						num_bg_hops_10k = len(background_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						lambda_10k = (num_bg_hops_10k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_10k,1))
						lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])


						#record type of lambda used
						index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
						lambda_type_list.append(list_of_l_names[index])
						#record lambda
						lambda_list.append(lambda_f)
						#compute pvalue and record it

						pvalue = 1-scistat.poisson.cdf((num_exp_hops+pseudocounts),lambda_f*max(num_TTAAs,1)+pseudocounts)
						pvalue_list.append(pvalue)

					else: #scale experiment hops down
						#compute lambda bg
						num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
						lambda_bg = (float(num_bg_hops)/max(num_TTAAs,1)) 



						#compute lambda 1k
						num_bg_hops_1k = len(background_dict_of_trees[name].find(peak_center-499,peak_center+500))
						num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(peak_center-499,peak_center+500))
						lambda_1k = (float(num_bg_hops_1k)/(max(num_TTAAs_1k,1)))



						#compute lambda 5k
						num_bg_hops_5k = len(background_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
						lambda_5k = (float(num_bg_hops_5k)/(max(num_TTAAs_5k,1)))



						#compute lambda 10k
						num_bg_hops_10k = len(background_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
						lambda_10k = (float(num_bg_hops_10k)/(max(num_TTAAs_10k,1)))
						lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])



						#record type of lambda used
						index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
						lambda_type_list.append(list_of_l_names[index])
						#record lambda
						lambda_list.append(lambda_f)
						#compute pvalue and record it
						pvalue = 1-scistat.poisson.cdf(((float(total_background_hops)/total_experiment_hops)*num_exp_hops+pseudocounts),lambda_f*max(num_TTAAs,1)+pseudocounts)
						pvalue_list.append(pvalue)

					#number of hops that are a user-defined distance from peak center
					sig_flag = 0

					

				#else do nothing.
				
				
	#make frame from all of the lists
	peaks_frame["Chr"] = chr_list
	peaks_frame["Start"] = start_list
	peaks_frame["End"] = end_list
	peaks_frame["Center"] = center_list
	peaks_frame["Experiment Hops"] = num_exp_hops_list 
	peaks_frame["Fraction Experiment"] = frac_exp_list 
	peaks_frame["Background Hops"] = num_bg_hops_list 
	peaks_frame["Fraction Background"] = frac_bg_list
	peaks_frame["Lambda Type"] = lambda_type_list
	peaks_frame["Lambda"] = lambda_list
	peaks_frame["Poisson pvalue"] = pvalue_list
	return peaks_frame


def compute_cumulative_poisson(exp_hops_region,bg_hops_region,total_exp_hops,total_bg_hops,pseudocounts):
	#usage
	#scistat.poisson.cdf(x,mu)
	#scales sample with more hops down to sample with less hops
	#tested 6/14/17
	if total_bg_hops >= total_exp_hops:
		return(1-scistat.poisson.cdf((exp_hops_region+pseudocounts),bg_hops_region * (float(total_exp_hops)/float(total_bg_hops)) + pseudocounts))
	else:
		return(1-scistat.poisson.cdf(((exp_hops_region *(float(total_bg_hops)/float(total_exp_hops)) )+pseudocounts),bg_hops_region + pseudocounts))

def find_peaks_bf(experiment_gnashyframe,TTAA_frame,pvalue_cutoff = 1e-3,window_size = 1000, step_size = 500,pseudocounts = 0.2):
	"""This function is the same as find_peaks but it calls peaks without using
	a background distribution.  To do so, it scans through the genome
	with a window of window_size and step size of step_size and looks for regions that
	have signficantly more experiment hops in the window than expected from the number of
	experimenta hops in a 10kb window.
	It merges consecutively enriched windows and computes the center of the peak.  Next 
	it computes lambda, the number of insertions per TTAA expected from the experimental
	distribution taking the max of lamda_5k and lambda_10k, where the expected number of hops are
	estimated from the number of hops in a 5k or 10k window around the peak center.  It then computes
	a p-value based on the expected number of hops = lamda * number of TTAAs in peak * number of hops
	in peak.  Finally, it returns a frame that has Chr,Start,End,Center,Experiment Hops,
	Fraction Experiment Lambda Type, Lambda,Poisson pvalue

	"""
	peaks_frame = pd.DataFrame(columns = ["Chr","Start","End","Center","Experiment Hops",
		"Fraction Experiment","Lambda Type",
		"Lambda","Poisson pvalue"])

	
	experiment_gnashy_dict = {}
	experiment_dict_of_trees = {}
	total_experiment_hops = len(experiment_gnashyframe)
	TTAA_frame_gbChr_dict = {} 
	TTAA_dict_of_trees = {}
	list_of_l_names = ["5k_bf","10k_bf"]

	#group by chromosome and populate interval tree with TTAA positions
	for name,group in TTAA_frame.groupby('Chr'):
		 
		TTAA_frame_gbChr_dict[name] = group
		TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
		#initialize tree
		TTAA_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in TTAA_frame_gbChr_dict[name].iterrows():	
			TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
        print "I have created the TTAA interval tree"
	#group by chromosome and populate interval tree with positions of experiment hops
	for name,group in experiment_gnashyframe.groupby('Chr'):
		experiment_gnashy_dict[name] = group
		experiment_gnashy_dict[name].index = experiment_gnashy_dict[name]["Pos"]
		#initialize tree
		experiment_dict_of_trees[name] = Intersecter() 
		#populate tree with position as interval
		for idx, row in experiment_gnashy_dict[name].iterrows():	
			experiment_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero
        print "I have created the experiment interval tree"
	#these will eventually be the columns in the peaks frame that will be returned.
	chr_list = []
	start_list = []
	end_list = []
	center_list = []
	num_exp_hops_list = []
	frac_exp_list = []
	lambda_type_list =[]
	lambda_list = []
	pvalue_list = []
	
	#group experiment gnashyfile by chomosome
	for name,group in experiment_gnashyframe.groupby('Chr'):
		max_pos = max(group["Pos"])
		sig_start = 0
		sig_end = 0
		sig_flag = 0
		for window_start in range(5000,max_pos+window_size+5001,step_size):
			overlap = experiment_dict_of_trees[name].find(window_start,window_start+window_size - 1)
			num_exp_hops = len(overlap)
			overlap_10k = experiment_dict_of_trees[name].find(window_start-4999,window_start+window_size +5000 - 1)
			num_10k_hops = len(overlap_10k)
			num_TTAAs_window = len(TTAA_dict_of_trees[name].find(window_start,window_start+window_size - 1))
			num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(window_start-4999,window_start+window_size +5000 - 1))

			lambda_10k = (num_10k_hops/(max(num_TTAAs_10k,1))) #expected number of hops per TTAA

			#is this window significant?
                        pvalue = 1-scistat.poisson.cdf((num_exp_hops+pseudocounts),lambda_10k*max(num_TTAAs_window,1)+pseudocounts)
			if pvalue < pvalue_cutoff:
				#was last window significant?
				if sig_flag:
					#if so, extend end of windows
					sig_end = window_start+window_size-1
				else:
					#otherwise, define new start and end and set flag
					sig_start = window_start
					sig_end = window_start+window_size-1
					sig_flag = 1

			else:
				#current window not significant.  Was last window significant?
				if sig_flag:
					#add full sig window to the frame of peaks 
					
					#add chr, peak start, peak end
					chr_list.append(name) #add chr to frame
					start_list.append(sig_start) #add peak start to frame
					end_list.append(sig_end) #add peak end to frame
				
					#compute peak center and add to frame
					overlap = experiment_dict_of_trees[name].find(sig_start,sig_end)
					exp_hop_pos_list = [x.start for x in overlap]
					peak_center = np.median(exp_hop_pos_list)
					center_list.append(peak_center) #add peak center to frame

					#add number of experiment hops in peak to frame
					num_exp_hops = len(overlap)
					num_exp_hops_list.append(num_exp_hops)

					#add fraction of experiment hops in peak to frame
					frac_exp_list.append(float(num_exp_hops)/total_experiment_hops)

					num_TTAAs_peak = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
                                        print "Start: "+str(sig_start)
                                        print "End: "+ str(sig_end)
                                        print "Exp hops: "+str(num_exp_hops)
                                        print "TTAAs: " + str(num_TTAAs_peak)
					#compute lambda 5k
					num_exp_hops_5k = len(experiment_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
					num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(peak_center-2499,peak_center+2500))
					lambda_5k = float(num_exp_hops_5k)/(max(num_TTAAs_5k,1))

					#compute lambda 10k
					num_exp_hops_10k = len(experiment_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
					num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(peak_center-4999,peak_center+5000))
					lambda_10k = float(num_exp_hops_10k)/(max(num_TTAAs_10k,1))
					lambda_f = max([lambda_5k,lambda_10k])


					#record type of lambda used
					index = [lambda_5k,lambda_10k].index(max([lambda_5k,lambda_10k]))
					lambda_type_list.append(list_of_l_names[index])
					#record lambda
					lambda_list.append(lambda_f)
					#compute pvalue and record it

					pvalue = 1-scistat.poisson.cdf((num_exp_hops+pseudocounts),lambda_f*max(num_TTAAs_peak,1)+pseudocounts)
					pvalue_list.append(pvalue)              
                                        print "pvalue: "+str(pvalue)
					#number of hops that are a user-defined distance from peak center
					sig_flag = 0			

				#else do nothing.
				
				
	#make frame from all of the lists
	peaks_frame["Chr"] = chr_list
	peaks_frame["Start"] = start_list
	peaks_frame["End"] = end_list
	peaks_frame["Center"] = center_list
	peaks_frame["Experiment Hops"] = num_exp_hops_list 
	peaks_frame["Fraction Experiment"] = frac_exp_list 
	peaks_frame["Lambda Type"] = lambda_type_list
	peaks_frame["Lambda"] = lambda_list
	peaks_frame["Poisson pvalue"] = pvalue_list
	return peaks_frame


def add_pvalues_to_cluster_frame_macs(cluster_frame,background_gnashyframe,TTAA_frame,total_experiment_hops,pseudocounts = 0.2,macs_pvalue=True):
    background_gnashy_dict = {} 
    background_dict_of_trees = {}
    total_background_hops = len(background_gnashyframe)
    TTAA_frame_gbChr_dict = {} 
    TTAA_dict_of_trees = {}
    list_of_l_names = ["bg","1k","5k","10k"]

    print "working on it..."
    #make interval tree for TTAAs
    for name,group in TTAA_frame.groupby('Chr'): 
        TTAA_frame_gbChr_dict[name] = group
        TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
        #initialize tree
        TTAA_dict_of_trees[name] = Intersecter() 
        #populate tree with position as interval
        for idx, row in TTAA_frame_gbChr_dict[name].iterrows(): 
            TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
    print "Made TTAA interval tree"
    #group by chromosome and populate interval tree with positions of background hops
    for name,group in background_gnashyframe.groupby('Chr'):
        background_gnashy_dict[name] = group
        background_gnashy_dict[name].index = background_gnashy_dict[name]["Pos"]
        #initialize tree
        background_dict_of_trees[name] = Intersecter() 
        #populate tree with position as interval
        for idx, row in background_gnashy_dict[name].iterrows():    
            background_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) #may need to be one.  Try with zero
    print "Made Background tree"
    #go through cluster frame and compute pvalues 
    num_bg_hops_list =[]
    frac_bg_list = []
    lambda_type_list =[]
    lambda_list = []
    pvalue_list = []
    for idx,row in cluster_frame.iterrows():
        #add number of background hops in cluster to frame
        cluster_center = row["Center"]
        bg_overlap = background_dict_of_trees[row["Chr"]].find(row["Start"],row["End"])
        num_bg_hops = len(bg_overlap)
        num_bg_hops_list.append(num_bg_hops)
        #add fraction background 
        frac_bg_list.append(float(num_bg_hops)/total_background_hops)       
        #find lambda and compute significance of cluster
        print row["Chr"]
        print row["Start"]
        print row["End"]
        print "background hops "+str(len(bg_overlap))
        print row["Experiment Hops"]

        #replace num_exp hops with row["Experiment Hops"]

        if total_background_hops >= total_experiment_hops: #scale bg hops down
            #compute lambda bg
            num_TTAAs = len(TTAA_dict_of_trees[name].find(row["Start"],row["End"]))
            lambda_bg = ((num_bg_hops*(float(total_experiment_hops)/total_background_hops))/max(num_TTAAs,1)) 

            #compute lambda 1k
            num_bg_hops_1k = len(background_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            lambda_1k = (num_bg_hops_1k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_1k,1))


            #compute lambda 5k
            num_bg_hops_5k = len(background_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            lambda_5k = (num_bg_hops_5k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_5k,1))


            #compute lambda 10k
            num_bg_hops_10k = len(background_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            lambda_10k = (num_bg_hops_10k*(float(total_experiment_hops)/total_background_hops))/(max(num_TTAAs_10k,1))
            if macs_pvalue:
                lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])
                index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
                lambda_type_list.append(list_of_l_names[index])
            else:
                lambda_f = lambda_bg
                lambda_type_list.append(list_of_l_names[0])
            lambda_list.append(lambda_f)
            #compute pvalue and record it
            pvalue = 1-scistat.poisson.cdf((row["Experiment Hops"]+pseudocounts),lambda_f*max(num_TTAAs,1)+pseudocounts)
            pvalue_list.append(pvalue)

        else: #scale experiment hops down
            #compute lambda bg
            num_TTAAs = len(TTAA_dict_of_trees[name].find(sig_start,sig_end))
            lambda_bg = (float(num_bg_hops)/max(num_TTAAs,1)) 

            #compute lambda 1k
            num_bg_hops_1k = len(background_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            num_TTAAs_1k = len(TTAA_dict_of_trees[name].find(cluster_center-499,cluster_center+500))
            lambda_1k = (float(num_bg_hops_1k)/(max(num_TTAAs_1k,1)))

            #compute lambda 5k
            num_bg_hops_5k = len(background_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            num_TTAAs_5k = len(TTAA_dict_of_trees[name].find(cluster_center-2499,cluster_center+2500))
            lambda_5k = (float(num_bg_hops_5k)/(max(num_TTAAs_5k,1)))

            #compute lambda 10k
            num_bg_hops_10k = len(background_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            num_TTAAs_10k = len(TTAA_dict_of_trees[name].find(cluster_center-4999,cluster_center+5000))
            lambda_10k = (float(num_bg_hops_10k)/(max(num_TTAAs_10k,1)))
            
            if macs_pvalue:
                lambda_f = max([lambda_bg,lambda_1k,lambda_5k,lambda_10k])
                index = [lambda_bg,lambda_1k,lambda_5k,lambda_10k].index(max([lambda_bg,lambda_1k,lambda_5k,lambda_10k]))
                lambda_type_list.append(list_of_l_names[index])
            else:
                lambda_f = lambda_bg
                lambda_type_list.append(list_of_l_names[0])

            #compute pvalue and record it
            pvalue = 1-scistat.poisson.cdf(((float(total_background_hops)/row["Experiment Hops"])*num_exp_hops+pseudocounts),lambda_f*max(num_TTAAs,1)+pseudocounts)
            pvalue_list.append(pvalue)

            #else do nothing.
                         
    #make frame from all of the lists 
    cluster_frame["Background Hops"] = num_bg_hops_list 
    cluster_frame["Fraction Background"] = frac_bg_list
    cluster_frame["Lambda Type"] = lambda_type_list
    cluster_frame["Lambda"] = lambda_list
    cluster_frame["Poisson pvalue"] = pvalue_list
    return cluster_frame

def add_pvalues_to_cluster_frame_macs_bf(cluster_frame,experiment_gnashyframe,TTAA_frame,pseudocounts = 0.2,macs_pvalue=True):
    experiment_gnashy_dict = {}
    experiment_dict_of_trees = {}
    TTAA_frame_gbChr_dict = {} 
    TTAA_dict_of_trees = {}
    list_of_l_names = ["5k_bf","10k_bf"]
    print "Making interval tree for experiment hops..."
    for name,group in experiment_gnashyframe.groupby('Chr'):
        experiment_gnashy_dict[name] = group
        experiment_gnashy_dict[name].index = experiment_gnashy_dict[name]["Pos"]
        #initialize tree
        experiment_dict_of_trees[name] = Intersecter()
        #populate tree with position as interval
        for idx, row in experiment_gnashy_dict[name].iterrows():
            experiment_dict_of_trees[name].add_interval(Interval(int(idx),int(idx)+1)) 
    print "Making interval tree for TTAAs..."
    #make interval tree for TTAAs
    for name,group in TTAA_frame.groupby('Chr'): 
        TTAA_frame_gbChr_dict[name] = group
        TTAA_frame_gbChr_dict[name].index = TTAA_frame_gbChr_dict[name]["Pos"]
        #initialize tree
        TTAA_dict_of_trees[name] = Intersecter() 
        #populate tree with position as interval
        for idx, row in TTAA_frame_gbChr_dict[name].iterrows(): 
            TTAA_dict_of_trees[name].add_interval(Interval(int(idx),int(idx+4)))
    #go through cluster frame and compute pvalues 
    lambda_type_list =[]
    lambda_list = []
    pvalue_list = []
    for idx,row in cluster_frame.iterrows():
        #add number of background hops in cluster to frame
        cluster_center = row["Center"]
        #find lambda and compute significance of cluster
        print row["Chr"]
        print row["Start"]
        print row["End"]
        num_TTAAs = len(TTAA_dict_of_trees[row["Chr"]].find(row["Start"],row["End"]))
        #compute lambda 5k
        num_exp_hops_5k = len(experiment_dict_of_trees[row["Chr"]].find(cluster_center-2499,cluster_center+2500))
        num_TTAAs_5k = len(TTAA_dict_of_trees[row["Chr"]].find(cluster_center-2499,cluster_center+2500))
        lambda_5k = float(num_exp_hops_5k)/(max(num_TTAAs_5k,1))

        #compute lambda 10k
        num_exp_hops_10k = len(experiment_dict_of_trees[row["Chr"]].find(cluster_center-4999,cluster_center+5000))
        num_TTAAs_10k = len(TTAA_dict_of_trees[row["Chr"]].find(cluster_center-4999,cluster_center+5000))
        lambda_10k = float(num_exp_hops_10k)/(max(num_TTAAs_10k,1))
        if macs_pvalue:
            lambda_f = max([lambda_5k,lambda_10k])
            index = [lambda_5k,lambda_10k].index(max([lambda_5k,lambda_10k]))
            lambda_type_list.append(list_of_l_names[index])
        else:
            lambda_f = lambda_10k
            lambda_type_list.append(list_of_l_names[1])
        lambda_list.append(lambda_f)
        #compute pvalue and record it
        pvalue = 1-scistat.poisson.cdf((row["Experiment Hops"]+pseudocounts),lambda_f*max(num_TTAAs,1)+pseudocounts)
        pvalue_list.append(pvalue)

                         
    #make frame from all of the lists 
    cluster_frame["Lambda Type"] = lambda_type_list
    cluster_frame["Lambda"] = lambda_list
    cluster_frame["Poisson pvalue"] = pvalue_list
    return cluster_frame

def peaks_frame_to_bed(peaks_frame,bedfilename,genome_type='hg19'):
	bed_frame = peaks_frame[["Chr","Start","End"]].copy()
	bed_frame.loc[:,"Start"] = bed_frame["Start"]-1 #start coords of bed files are 0 indexed while ends are 1 indexed
	if (genome_type == 'hg18') or (genome_type == 'hg19'):
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
	else:
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
	bed_frame.loc[:,"Chr"] = [chr_dict[x] for x in bed_frame.loc[:,"Chr"]]
	bed_frame.to_csv(bedfilename,sep = "\t",header = None,index=None)


#def merge_bedfile_and_peaks_frame():

def annotate_peaks_frame(peaks_frame,refGene_filename = '/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',genome_type='mm10'):
	"This function annotates peaks using pybedtools"

	peaks_frame_to_bed(peaks_frame,"temp_peaks.bed",genome_type)
	peaks_bed = bt.BedTool('temp_peaks.bed')
	peaks_bed = peaks_bed.sort()
	#convert peaks frame to bed
	peaks_bed = peaks_bed.closest('/scratch/rmlab/ref/calling_card_ref/mouse/refGene.mm10.Sorted.bed',D="ref",t="first",k=2)
	peaks_bed.saveas('temp_peaks.bed')

	#read in gene_annotation_bedfilename
	temp_annotated_peaks = pd.read_csv("temp_peaks.bed",delimiter = "\t",header=None)
	temp_annotated_peaks.columns = ["Chr","Start","End","Feature Chr","Feature Start", "Feature End","Feature sName","Feature Name","Strand","Distance"]
	temp_annotated_peaks.loc[:,"Start"] = temp_annotated_peaks["Start"]+1 #convert start coords back to 1 indexed coordinates
	index_list = [(x,y,z) for x,y,z in zip(temp_annotated_peaks["Chr"],temp_annotated_peaks["Start"],temp_annotated_peaks["End"])]

	temp_annotated_peaks.index = pd.MultiIndex.from_tuples(index_list)
	
	if (genome_type == 'hg18') or (genome_type == 'hg19'):
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chr20',21:'chr21',22:'chr22',23:'chrX',24:'chrY'}
	else:
		chr_dict = {1:'chr1',2:'chr2',3:'chr3',4:'chr4',5:'chr5',6:'chr6',7:'chr7',8:'chr8',9:'chr9',10:'chr10',11:'chr11',12:'chr12',13:'chr13',14:'chr14',15:'chr15',16:'chr16',17:'chr17',18:'chr18',19:'chr19',20:'chrX',21:'chrY'}
	peaks_frame.loc[:,"Chr"] = [chr_dict[x] for x in peaks_frame.loc[:,"Chr"]]
	
	index_list = [(x,y,z) for x,y,z in zip(peaks_frame["Chr"],peaks_frame["Start"],peaks_frame["End"])]
	peaks_frame.index = pd.MultiIndex.from_tuples(index_list)
        peaks_frame = peaks_frame.sortlevel(0,axis=1)
	temp_list = list(peaks_frame.columns)
	temp_list = temp_list+["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand","Feature 1 Distance", 
		"Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand","Feature 2 Distance"]
	peaks_frame = peaks_frame.reindex(columns=temp_list,fill_value=0)

	for idx,row in temp_annotated_peaks.iterrows():
		if not(peaks_frame.loc[idx,"Feature 1 sName"]):
			peaks_frame.loc[idx,["Feature 1 sName","Feature 1 Name","Feature 1 Start","Feature 1 End","Feature 1 Strand",
			"Feature 1 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]])	
		else:
			peaks_frame.loc[idx,["Feature 2 sName","Feature 2 Name","Feature 2 Start","Feature 2 End","Feature 2 Strand",
			"Feature 2 Distance"]] = list(row[["Feature sName","Feature Name","Feature Start", "Feature End","Strand","Distance"]])
	return peaks_frame		
	
