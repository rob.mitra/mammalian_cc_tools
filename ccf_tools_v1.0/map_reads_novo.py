#!/usr/bin/env python
"""
map_reads_novo.py
written 7/25/17
usage

map_reads -f <base file name> -g <genome> -p <paired end flag> 
-t transponson sequence to trim -l <length of sequence 3' to transposon
 to map> -q <quality filter> -o <output path>

required
-f <base file name> -g <genome> 

not required
-p <paired end flag> default = False 
-t transposon sequence to trim default = TTTACGCAGACTATCTTTCTAGGG
-l <length of sequence 3' to transposon to map> default = all
-q <quality filter>  default = 10
-o <output path> default = ../output_and_analysis

This program requires that the ccf_tools module is loaded.
    """

import argparse
import ccf_tools
import os


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='map_reads.py')
    parser.add_argument('-f','--basename',help='base filename (full path)',required=True)
    parser.add_argument('-g','--genome',help='genome name',required=True)
    parser.add_argument('-p','--paired',help='paired read flag',required=False,default=False)
    parser.add_argument('-t','--transposon_sequence',help='transposon sequence',default="TTTACGCAGACTATCTTTCTAGGG")
    parser.add_argument('-l','--length',help='length of read to map',required=False,default=250)
    parser.add_argument('-q','--quality',help='quality score cutoff',required=False,default=10)
    parser.add_argument('-o','--outpath',help='output path',required=False,default='../output_and_analysis')
    args = parser.parse_args()
    if args.paired == "False":
        args.paired=False
    if not args.outpath[-1] == "/":
        args.outpath = args.outpath+"/"
    os.chdir(args.outpath)
    ccf_tools.map_reads_novo(args.basename,args.genome,args.paired,args.transposon_sequence,args.length,args.quality,args.outpath)

