import pysam
import pandas as pd
import csv
import re
import argparse

def read_barcode_file(barcode_filename):
	#This helper function reads in the experiment name, the primer barcode, and the transposon barcodes
	#from a file which is in the following format:
	#expt name \t primer barcode \t transposon barcode 1,transposon barcode 2, transposon barcode 3 etc.
	#It then returns a dictionary with key = a tuple (primer barcode, transposon barcode), value = expt_name
	#The last line of the file should not have a return character
	reader = csv.reader(open(barcode_filename, 'r'),delimiter = '\t')
	d = {}
	for row in reader:
		exname,b2 = row
		for transposon_barcode in b2.split(","):
			d[transposon_barcode]=exname
	return d
	
def sort_ccf_file(ccffilename,chrlist):
	ccf_frame = pd.read_csv(ccffilename,delimiter='\t',header=None,names=['Chr','Start','End','Reads','Strand','Barcode'])
	ccf_frame["Chr"] = pd.Categorical(ccf_frame["Chr"],chrlist)
	ccf_frame = ccf_frame.sort_values(['Chr','Start','End'])
	ccf_frame.to_csv(ccffilename,sep='\t',header=False,index=False)
	return [len(ccf_frame),ccf_frame.Reads.sum()]

def make_ccffile_yen(genome="mm10",bcfilename="../scripts/barcodes.txt",outpath="../output_and_analysis/"):
	"""	make_ccffile <bcfilename> <outpath>
		
		required
		none

		not required
		<genome>, default = "mm10"
		<bcfilename>, default = ../output_and_analysis/barcodes.txt  path and name of barcode file
		<outpath>, default = ../output_and_analysis/  path for input and output

		verified as OK 7/25/17 by RDM
	"""
	pattern = "^hg"
	if re.search(pattern,genome,re.I):
		chr_list = ['chr1','chr2','chr3','chr4','chr5',
		'chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chr20','chr21','chr22','chrX','chrY']
		print "making human ccffile"
	else:
		chr_list = ['chr1','chr2','chr3','chr4','chr5','chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chrX','chrY']
		print "making mouse ccffile"

	barcode_dict = read_barcode_file(bcfilename)
	#read experiments and barcodes into dictionary  
	#Key = (primer barcode, Transposon barode)
	#Value = expt name
	
	qc_dict = {}  #initialize quality control dictionary

	#LOOP THROUGH EXPERIMENTS
	#Loop through experiments and make a separate ccf file for each
	for expt in list(set(barcode_dict.values())):
		#for each experiment, there will be multiple bam files.  Loop through all of them
		#open output ccffile
		print "Analyzing "+expt
		output_filename = outpath+expt+".ccf"
		output_handle = file(output_filename, 'w') 
		#LOOP THROUGH BAM FILES CORRESPONDING TO 1 experiment
		for key in barcode_dict.keys(): #this could be made more efficient, but its more clear this way
			if barcode_dict[key] == expt:
				transposonBC = key
				basename = outpath+expt+"_"+transposonBC
				sbamFilename = basename+".sorted"
				pysam.sort(basename+".bam",sbamFilename)
				#sort and index bamfile
				sbamFilename = sbamFilename+".bam"
		   
				pysam.index(sbamFilename)
				print sbamFilename
				#inialize ccf dictionary
				ccf_dict = {}
				#make AlignmentFile object
				current_bamfile = pysam.AlignmentFile(sbamFilename,"rb")

				#loop through the chromosomes and pileup start sites
				for chr in chr_list:
					aligned_reads_group = current_bamfile.fetch(chr)
					#now loop through each read and pile up start sites
					for aread in aligned_reads_group:
						#is the read a reverse read?
						if aread.is_reverse:
							#does it align to a ttaa?
							if (aread.query_sequence[-4:]=='TTAA' or aread.query_sequence[-4:]=='ttaa'):
								#if so, get position and update dictionary
								startpos = aread.get_reference_positions()[-1] + 1 - 3 #zero indexed, but I want 1 indexed for output
								endpos = startpos + 3
								strand = "-"
								if (chr,startpos,endpos,strand) in ccf_dict:
									ccf_dict[(chr,startpos,endpos,strand)]+=1
								else:
									ccf_dict[(chr,startpos,endpos,strand)]=1
						else: #forward read
							#does it align to a ttaa?
							if (aread.query_sequence[0:4]=='TTAA' or aread.query_sequence[0:4]=='ttaa'):
								#if so, get position and update dicitonary
								startpos = aread.get_reference_positions()[0] + 1 #zero indexed, but I want 1 indexed for output
								endpos = startpos + 3
								strand = "+"
								if (chr,startpos,endpos,strand) in ccf_dict:
									ccf_dict[(chr,startpos,endpos,strand)]+=1
								else:
									ccf_dict[(chr,startpos,endpos,strand)]=1
				#we've gone through all reads on all chromsomes, so output the dictionary to ccf file
				for key in ccf_dict:
					output_handle.write("%s\t%s\t%s\t%s\t%s\t%s\n" %(key[0],key[1],key[2],ccf_dict[key],key[3],transposonBC))
		#we've gone through all barcodes in this experiment so close the output file
		output_handle.close()
		#OPEN ccf FILE AND SORT BY CHR THEN POS
		qc_dict[expt] = sort_ccf_file(output_filename,chr_list)
	#after all experiments have been analyzed, print out qc
	qc_handle = file(outpath+"ccfQC.txt",'w')
	for key in qc_dict:
		qc_handle.write("%s\t%s\t%s\n" %(key,qc_dict[key][0], qc_dict[key][1] ))
	qc_handle.close()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='make_gnashyfile.py')
	parser.add_argument('-b','--barcodefile',help='barcode filename (full path)',required=False,default='../scripts/barcodes.txt')
	parser.add_argument('-p','--outputpath',help='output path',required=False,default='../output_and_analysis')
	parser.add_argument('-g','--genome',help='genome to be analyzed',required=True)
	args = parser.parse_args()
	if not args.outputpath[-1] == "/":
		args.outputpath = args.outputpath+"/"
	make_ccffile_yen(args.genome,args.barcodefile,args.outputpath)
