import os
import re


file_list = os.listdir("./")

#zip and index bed fies
for file_name in file_list:
		if re.search(r'.bed$',file_name,re.I):
			'''added one line to avoid ._ for file name'''
			if file_name[0]!=".":
				cmd = "/opt/apps/htslib/1.2.1/bin/bgzip "+file_name
				os.system(cmd)
				cmd2 = "/opt/apps/htslib/1.2.1/bin/tabix -p bed "+file_name+".gz"
				os.system(cmd2)

#zip and index bedGraph fies
for file_name in file_list:
		if re.search(r'.bedGraph$',file_name,re.I):
			'''added one line to avoid ._ for file name'''
			if file_name[0]!=".":
				cmd = "/opt/apps/htslib/1.2.1/bin/bgzip "+file_name
				os.system(cmd)
				cmd2 = "/opt/apps/htslib/1.2.1/bin/tabix -p bed "+file_name+".gz"
				os.system(cmd2)
#zip ccf files
for file_name in file_list:
		if re.search(r'.ccf$',file_name,re.I):
			'''added one line to avoid ._ for file name'''
			if file_name[0]!=".":
				cmd = "/opt/apps/htslib/1.2.1/bin/bgzip "+file_name
				os.system(cmd)
				cmd2 = "/opt/apps/htslib/1.2.1/bin/tabix -p bed "+file_name+".gz"
				os.system(cmd2)
