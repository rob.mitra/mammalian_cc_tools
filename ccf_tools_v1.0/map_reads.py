"""
map_reads.py
written 7/25/17 by RDM
usage
map_reads -f <base file name> -g <genome> -p <paired end flag> 
-ft <five primer bases to trim> -tt <three primer bases to trim>
-q <quality filter> -o <output path>

required
-f <base file name> -g <genome> 

not required
-p <paired end flag> default = False 
-ft <five primer bases to trim> default =37  
-tt <three primer bases to trim> default = 0
-q <quality filter>  default = 10
-o <output path> default = ../output_and_analysis



This program requires that the bowtie2 module is loaded and
that the following environment variable is set:
    export BOWTIE2_INDEXES='/scratch/rmlab/ref/bowtie2_indexes/mm10'

    """

import argparse
import ccf_tools
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='map_reads.py')
    parser.add_argument('-f','--basename',help='base filename (full path)',required=True)
    parser.add_argument('-g','--genome',help='genome name',required=True)
    parser.add_argument('-p','--paired',help='paired read flag',required=False,default=False)
    parser.add_argument('-ft','--ftrim',help='five prime bases to trim',default=37)
    parser.add_argument('-tt','--ttrim',help='three prime bases to trim',required=False,default=0)
    parser.add_argument('-q','--quality',help='quality score cutoff',required=False,default=10)
    parser.add_argument('-o','--outpath',help='output path',required=False,default='../output_and_analysis')
    args = parser.parse_args()
    if args.paired == "False":
        args.paired=False
    if not args.outpath[-1] == "/":
        args.outpath = args.outpath+"/"
    os.chdir(args.outpath)
    ccf_tools.map_reads(args.basename,args.genome,args.paired,args.ftrim,args.ttrim,args.quality,args.outpath)

